Summary
=======
Library for determining a domain based on a root zone, sub domain, and port
regular expressions, and weighting.

Roadmap
=======
* 0.4.1: Current
 
 - Split up db lib into package
 
 - Split out lib unittest so all the test don't need to be duplicated when 
   implementing new databases

* 0.4.0:
 
 - Create unit test for deleteing, disabling, and setting a domain as default
 
 - Release to pypi instance

* 0.3.1: 

 - Add method to set an enabled/not deleted domain as default for a root zone
   and turns default off for all other domains on that root zone including
   disabled and deleted
 
 - Add method to disable a domain and enforce that another domain in the root
   zone is default
 
 - Add method to delete a domain and enforce that another domain in the root
   zone is default

* 0.3.0:
 
 - Create unit test for finding a domain that resolves a given host
 
 - Release to pypi instance

* 0.2.2:

 - Add method to get a domain that resolves a given host

* 0.2.1:

 - Adjust db library methods that do not require database interaction to include
   filtering by deleted and disabled

 - Adjust google app engine unittest to take into account deleted and disabled
   properties
 
 - Increase domainlib.db.lib, domainlib.db.model and app engine unittest to 100%
   coverage

* 0.2.0:
 
 - Create unit test for domain dal
 
 - Release to pypi instance

* 0.1.2: 

 - Create DAL for google app engine

* 0.1.1:
 
 - Create a database domain model spec with additional information for relating
   domains
   
 - Create method to get a domain from a iterable of domains by its internal id
 
 - Create method to get a domain from a iterable of domains by its public id
 
 - Define a database abstraction layer (DAL) which can
 
 	1. Perform CRUD operations
 	
 	2. Get a iterable of domains by their non-regex root zone
 	
 	3. Get a domain by its internal id
 	
 	4. Get a domain by its public id

* 0.1.0:

 - Create unit test for instance matching functions
 
 - Release to pypi instance

* 0.0.2:

 - Create a non-db model that defines a regular expression for root zones,
   sub domains, and ports
 
 - Create method to determine domain from a iterable of domain models using
   only a domain, the models domain/sub domain, and weight

* 0.0.1:

 - Create setup.py for use with easy_install and pip