from __future__ import unicode_literals
import os, inspect


"""
Defines the absolute path of the package.  This is used instead of __dir__
because __dir__ may not be defined when testing something in idle.
""" 
PACKAGE_PATH = os.path.dirname(os.path.abspath(
                       inspect.getframeinfo(inspect.currentframe()).filename))


"""
Defines the name of the package.  It is used by setup.py when creating eggs and
packaging source distributions.
"""
PACKAGE_NAME = "DomainLib"


"""
Defines the verion of the package.  It is used by setup.py when creating eggs.
"""
VERSION = "0.4.1"


__version__ = VERSION


"""
Defines the description of the package.  It is used by setup.py when creating
eggs.
"""
SHORT_DESCRIPTION = ("Library for determining a domain based on a root zone, "
                     "sub domain, and port regular expressions, and weighting.")


"""
Defines the long description of the package.
"""
LONG_DESCRIPTION = ""


try:
    #only really need this for distributions
    with open(os.path.join(PACKAGE_PATH, "../README.rst"), "r") as f:
        LONG_DESCRIPTION = f.readlines()

except:
    pass


"""
Defining the packages here so it can be used by other scripts to track
requirements.
"""
REQUIRED_PACKAGES = [
    "IdLib>=0.1"
]


"""
Defines all of the contributers to the library.  It is used by setup.py when
creating eggs.  Each author should be a tuple containing their name and email.
The first contributer in the list is used as the author in setup.py.
"""
CONTRIBUTERS = [
    ("Donald Ritter", "dritter@mediageneral.com"),
]


__author__ = CONTRIBUTERS[0][0]


"""
Defines any keywords that can be used when searching pypi to help find this
package.
"""
KEYWORDS = "domain site weight instance"