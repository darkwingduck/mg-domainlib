"""
Defines the google app engine specific Domain library
"""


from google.appengine.ext import db
from domainlib.db.models import Domain
from domainlib.db.impl.gae.models import Domain as DomainModel
import json
from datetime import datetime
import logging

log = logging.getLogger(__name__)


def _model_to_domain(result):
    """
    @summary: Takes the result from the database and loads its information into
    the abstracted domainlib.db.models.Domain.
    
    @type result: domainlib.db.impl.gae.models.Domain
    @param result: Row from the query being converted.
    
    @rtype: domainlib.db.models.Domain
    @return: Domain instance with loaded data or None if data could not be 
    loaded or is a version greater than this library can load.
    """
    
    if result:
        domain = Domain()
        domain._key = result
        domain.internal_id = result.internal_id
        domain.public_id = result.public_id
        domain.owner_id = result.owner_id
        domain.root_zone = result.root_zone
        domain.default = result.default
        domain.created = result.created
        domain.updated = result.updated
        domain.disabled = result.disabled
        domain.deleted = result.deleted
        
        if result.non_indexed_version == 1:
            data = json.loads(result.non_indexed_data)
            
            return _model_to_domain_v_1(domain=domain, data=data)
    
    return None


def _model_to_domain_v_1(domain, data):
    """
    @summary: Loads the data from a result for version 1 of the non_indexed_data
    structure.
    
    @type domain: domainlib.db.models.Domain
    @param domain: Domain instance data is being loaded into.
    
    @type data: Dict
    @param data: Dictionary holding the non-indexed data.
    
    @rtype: domainlib.db.models.Domain
    @return: Domain instance with loaded data.
    """
    
    domain.domain.root_zone = data.get("root_zone", "")
    domain.domain.sub_domain = data.get("sub_domain", "")
    domain.domain.port = data.get("port", r"(^[0-9]+|^)$")
    domain.domain.weight = data.get("weight", 0.0)
    
    return domain


def _domain_to_model(connection, domain, generate_internal_id,
                     validate_internal_id, generate_public_id,
                     validate_public_id, _fail_first_internal_id=False,
                     _fail_first_public_id=False):
    
    """
    @summary: Takes the domainlib.db.models.Domain and loads its information 
    into a model for the database.  The non_indexed_data will be written as the
    latest version.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type domain: domainlib.db.models.Domain
    @param domain: Domain instance with data to save.

    @type domain: domainlib.db.models.Domain
    @param domain: Domain instance being saved to the database.  If the domain
    does not have a _key, it will be created.
    
    @type generate_internal_id: Function
    @param generate_internal_id: Function used to generate a unique internal
    id for created domains.  This function takes no arguments and should return
    a unicode of 500 characters or less.
    
    This function will only be called if the internal_id has not been set yet.
    
    @type validate_internal_id: Function
    @param validate_internal_id: Function used to validate that a generated
    internal id is unique.  This function should take a connection and unicode
    and return True if the id is unique and False if it is not unique.
    
    This function will only be called if the internal_id has not been set yet.
    
    @type generate_public_id: Function
    @param generate_public_id: Function used to generate a unique public
    id for created domains.  This function takes no arguments and should return
    a unicode of 500 characters or less.
    
    This function will only be called if the public_id has not been set yet.
    
    @type validate_public_id: Function
    @param validate_public_id: Function used to validate that a generated
    public id is unique.  This function should take a connection and unicode
    and return True if the id is unique and False if it is not unique.
    
    This function will only be called if the public_id has not been set yet.
    
    @type _fail_first_internal_id: Boolean
    @param _fail_first_internal_id: Whether the first generated internal id
    should be treated as invalid.  This is only used for unittesting.
    
    @type _fail_first_public_id: Boolean
    @param _fail_first_public_id: Whether the first generated public id should
    be treated as invalid.  This is only used for unittesting.
    
    @rtype: domainlib.db.impl.gae.models.Domain
    @return: Row from the query being updated.
    """
    
    model = domain._key
    
    non_indexed_data = json.dumps(_domain_to_model_v_1(domain=domain))
    
    #creating the instance
    if not model:
        if not domain.internal_id:
            domain.internal_id = generate_internal_id()
            
            while (not validate_internal_id(connection, domain.internal_id)
                   or _fail_first_internal_id):
                
                if _fail_first_internal_id:
                    _fail_first_internal_id = False
                
                domain.internal_id = generate_internal_id()
        
        if not domain.public_id:
            domain.public_id = generate_public_id()
            
            while (not validate_public_id(connection, domain.public_id) or
                   _fail_first_public_id):
                
                if _fail_first_public_id:
                    _fail_first_public_id = False
                
                domain.public_id = generate_public_id()
        
        domain.created = datetime.now()
        domain.updated = datetime.now()
        
        model = DomainModel(
            internal_id=domain.internal_id,
            public_id=domain.public_id,
            owner_id=domain.owner_id,
            root_zone=domain.root_zone,
            default=domain.default,
            created=domain.created,
            updated=domain.updated,
            disabled=domain.disabled,
            deleted=domain.deleted,
            non_indexed_version=DomainModel.CURRENT_NON_INDEXED_DATA_VERSION,
            non_indexed_data=non_indexed_data)
    
    else:
        domain.updated = datetime.now()
        
        model.internal_id = domain.internal_id
        model.public_id = domain.public_id
        model.owner_id = domain.owner_id
        model.root_zone = domain.root_zone
        model.default = domain.default
        model.created = domain.created
        model.updated = domain.updated
        model.disabled = domain.disabled
        model.deleted = domain.deleted
        model.non_indexed_version = DomainModel.CURRENT_NON_INDEXED_DATA_VERSION
        model.non_indexed_data = non_indexed_data
    
    return model


def _domain_to_model_v_1(domain):
    """
    @summary: Loads the data from a result for version 1 of the non_indexed_data
    structure.
    
    @type domain: domainlib.db.models.Domain
    @param domain: Domain instance data is being saved from.
    
    @rtype: Dict
    @return: Dictionary holding the non-indexed data.
    """
    
    return {
        "root_zone" : domain.domain.root_zone,
        "sub_domain" : domain.domain.sub_domain,
        "port" : domain.domain.port,
        "weight" : domain.domain.weight
    }


def update(connection, domain, generate_internal_id, validate_internal_id,
           generate_public_id, validate_public_id,
           _fail_first_generated_ids=False):
    
    """
    @summary: Updates a Domain instance to the connected database.  The instance
    must exists before updating.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type domain: domainlib.db.models.Domain
    @param domain: Domain instance being saved to the database.  If the domain
    does not have a _key, it will be created.
    
    If the domain was created, it's internal_id and public_id will be created
    if they were not previously specified.
    
    @type domain: domainlib.db.models.Domain
    @param domain: Domain instance being saved to the database.  If the domain
    does not have a _key, it will be created.
    
    @type generate_internal_id: Function
    @param generate_internal_id: Function used to generate a unique internal
    id for created domains.  This function takes no arguments and should return
    a unicode of 500 characters or less.
    
    This function will only be called if the internal_id has not been set yet.
    
    @type validate_internal_id: Function
    @param validate_internal_id: Function used to validate that a generated
    internal id is unique.  This function should take a connection and unicode
    and return True if the id is unique and False if it is not unique.
    
    This function will only be called if the internal_id has not been set yet.
    
    @type generate_public_id: Function
    @param generate_public_id: Function used to generate a unique public
    id for created domains.  This function takes no arguments and should return
    a unicode of 500 characters or less.
    
    This function will only be called if the public_id has not been set yet.
    
    @type validate_public_id: Function
    @param validate_public_id: Function used to validate that a generated
    public id is unique.  This function should take a connection and unicode
    and return True if the id is unique and False if it is not unique.
    
    This function will only be called if the public_id has not been set yet.
    
    @type _fail_first_generated_ids: Boolean
    @param _fail_first_generated_ids: Whether to fail the first generated
    internal and public ids.  This is only used by unittests.
    
    @rtype: Boolean
    @return: Whether the instance could be saved or not.
    """
    
    key = None
    
    try:
        model = _domain_to_model(connection=connection,
                             domain=domain,
                             generate_internal_id=generate_internal_id,
                             validate_internal_id=validate_internal_id,
                             generate_public_id=generate_public_id,
                             validate_public_id=validate_public_id,
                             _fail_first_internal_id=_fail_first_generated_ids,
                             _fail_first_public_id=_fail_first_generated_ids)
        
        key = model.put()
    
    except Exception as e:
        log.error("Error updating Domain with Google App Engine:%s" % e)
    
    if key:
        if not domain._key:
            domain._key = model
        
        return True
    
    return False


def get_by_internal_id(connection, internal_id, deleted, disabled):
    """
    @summary: Gets a Domain from the database with the specified internal_id.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type internal_id: Unicode
    @param internal_id: Internal id of the domain to be retrieved.
    
    @type deleted: Boolean
    @param deleted: Whether the Domain has been marked as deleted.  Pass None
    if you don't care if it is deleted or not.
    
    @type disabled: Boolean
    @param disabled: Whether the Domain is currently disabled.  Pass None if you
    don't care if it is disabled or not.
    
    @rtype: domainlib.db.models.Domain
    @return: Loaded Domain model or None of not found.
    """
    
    query = db.Query(DomainModel).filter("internal_id ==", internal_id)
    
    if deleted is not None:
        query = query.filter("deleted ==", deleted)
    
    if disabled is not None:
        query = query.filter("disabled ==", disabled)
    
    result = None
    
    for r in query.fetch(1):
        result = r
    
    return _model_to_domain(result=result)


def get_by_internal_ids(connection, internal_ids, deleted, disabled):
    """
    @summary: Gets Domains from the database with the specified internal_id.
    
    This is done as a single query on all entities because we want to run one
    query rather than one query for each id.  GAE's IN operator performs one
    query per entry in the iterable up to 30 entries.  If we need more than 30,
    we would have to perform the filter manually so we may as well do it.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type internal_ids: Iterable
    @param internal_ids: Iterable of internal ids of the domain to be retrieved.
    
    @type deleted: Boolean
    @param deleted: Whether the Domain has been marked as deleted.  Pass None
    if you don't care if it is deleted or not.
    
    @type disabled: Boolean
    @param disabled: Whether the Domain is currently disabled.  Pass None if you
    don't care if it is disabled or not.
    
    @rtype: Dict
    @return: Dictionary mapping of Domains with the internal id as key.  If no
    domain could be loaded, it's value will be None.
    """
    
    query = db.Query(DomainModel)
    
    if deleted is not None:
        query = query.filter("deleted ==", deleted)
    
    if disabled is not None:
        query = query.filter("disabled ==", disabled)
    
    ids = dict.fromkeys(internal_ids)
    
    results = filter(lambda x:x.internal_id in internal_ids, map(None, query))
    
    for result in results:
        ids[result.internal_id] = _model_to_domain(result)
    
    return ids


def get_by_public_id(connection, public_id, deleted, disabled):
    """
    @summary: Gets a Domain from the database with the specified public_id.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type public_id: Unicode
    @param public_id: Public id of the domain to be retrieved.
    
    @type deleted: Boolean
    @param deleted: Whether the Domain has been marked as deleted.  Pass None
    if you don't care if it is deleted or not.
    
    @type disabled: Boolean
    @param disabled: Whether the Domain is currently disabled.  Pass None if you
    don't care if it is disabled or not.
    
    @rtype: domainlib.db.models.Domain
    @return: Loaded Domain model or None of not found.
    """
    
    query = db.Query(DomainModel).filter("public_id ==", public_id)
    
    if deleted is not None:
        query = query.filter("deleted ==", deleted)
    
    if disabled is not None:
        query = query.filter("disabled ==", disabled)
    
    result = None
    
    for r in query.fetch(1):
        result = r
    
    return _model_to_domain(result=result)


def get_by_public_ids(connection, public_ids, deleted, disabled):
    """
    @summary: Gets Domains from the database with the specified public_id.
    
    This is done as a single query on all entities because we want to run one
    query rather than one query for each id.  GAE's IN operator performs one
    query per entry in the iterable up to 30 entries.  If we need more than 30,
    we would have to perform the filter manually so we may as well do it.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type public_ids: Iterable
    @param public_ids: Iterable of public ids of the domain to be retrieved.
    
    @type deleted: Boolean
    @param deleted: Whether the Domain has been marked as deleted.  Pass None
    if you don't care if it is deleted or not.
    
    @type disabled: Boolean
    @param disabled: Whether the Domain is currently disabled.  Pass None if you
    don't care if it is disabled or not.
    
    @rtype: Dict
    @return: Dictionary mapping of Domains with the public id as key.  If no
    domain could be loaded, it's value will be None.
    """
    
    query = db.Query(DomainModel)
    
    if deleted is not None:
        query = query.filter("deleted ==", deleted)
    
    if disabled is not None:
        query = query.filter("disabled ==", disabled)
    
    ids = dict.fromkeys(public_ids)
    
    results = filter(lambda x:x.public_id in public_ids, map(None, query))
    
    for result in results:
        ids[result.public_id] = _model_to_domain(result)
    
    return ids


def get_by_owner_id(connection, owner_id, deleted, disabled):
    """
    @summary: Gets a set of Domains from the database with the specified
    owner_id.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type owner_id: Unicode
    @param owner_id: Owner id of the domains to be retrieved.
    
    @type deleted: Boolean
    @param deleted: Whether the Domain has been marked as deleted.  Pass None
    if you don't care if it is deleted or not.
    
    @type disabled: Boolean
    @param disabled: Whether the Domain is currently disabled.  Pass None if you
    don't care if it is disabled or not.
    
    @rtype: Set
    @return: Set of Domains that have the specified owner id.
    """
    
    query = db.Query(DomainModel).filter("owner_id ==", owner_id)
    
    if deleted is not None:
        query = query.filter("deleted ==", deleted)
    
    if disabled is not None:
        query = query.filter("disabled ==", disabled)
    
    results = set()
    
    for result in query:
        results.add(_model_to_domain(result=result))
    
    return results


def get_by_owner_ids(connection, owner_ids, deleted, disabled):
    """
    @summary: Gets Domains from the database with the specified owner_id.
    
    This is done as a single query on all entities because we want to run one
    query rather than one query for each id.  GAE's IN operator performs one
    query per entry in the iterable up to 30 entries.  If we need more than 30,
    we would have to perform the filter manually so we may as well do it.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type public_ids: Iterable
    @param public_ids: Iterable of public ids of the domain to be retrieved.
    
    @type deleted: Boolean
    @param deleted: Whether the Domain has been marked as deleted.  Pass None
    if you don't care if it is deleted or not.
    
    @type disabled: Boolean
    @param disabled: Whether the Domain is currently disabled.  Pass None if you
    don't care if it is disabled or not.
    
    @rtype: Dict
    @return: Dictionary mapping of Domains with the owner id as key and a set of
    loaded domains as value.  If no domain could be loaded, it's value will be
    an empty set.
    """
    
    query = db.Query(DomainModel)
    
    if deleted is not None:
        query = query.filter("deleted ==", deleted)
    
    if disabled is not None:
        query = query.filter("disabled ==", disabled)
    
    ids = dict((owner_ids[i], set()) for i in range(0, len(owner_ids)))
    
    results = filter(lambda x:x.owner_id in owner_ids, map(None, query))
    
    for result in results:
        ids[result.owner_id].add(_model_to_domain(result))
    
    return ids


def get_default_by_owner_id(connection, owner_id, deleted, disabled):
    """
    @summary: Gets the default Domain from the database with the specified
    owner_id.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type owner_id: Unicode
    @param owner_id: Owner id of the domains to be retrieved.
    
    @type deleted: Boolean
    @param deleted: Whether the Domain has been marked as deleted.  Pass None
    if you don't care if it is deleted or not.
    
    @type disabled: Boolean
    @param disabled: Whether the Domain is currently disabled.  Pass None if you
    don't care if it is disabled or not.
    
    @rtype: domainlib.db.models.Domain
    @return: Default Domains that has the specified owner id or None if no
    domain can be found.
    """
    
    query = db.Query(DomainModel).filter("owner_id ==", owner_id)
    
    if deleted is not None:
        query = query.filter("deleted ==", deleted)
    
    if disabled is not None:
        query = query.filter("disabled ==", disabled)
    
    query = query.filter("default ==", True)
    
    result = None
    
    for r in query.fetch(1):
        result = r
    
    return _model_to_domain(result=result)


def get_default_by_owner_ids(connection, owner_ids, deleted, disabled):
    """
    @summary: Gets Domains from the database with the specified owner_id.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type public_ids: Iterable
    @param public_ids: Iterable of public ids of the domain to be retrieved.
    
    @type deleted: Boolean
    @param deleted: Whether the Domain has been marked as deleted.  Pass None
    if you don't care if it is deleted or not.
    
    @type disabled: Boolean
    @param disabled: Whether the Domain is currently disabled.  Pass None if you
    don't care if it is disabled or not.
    
    @rtype: Dict
    @return: Dictionary mapping of Domains with the owner id as key and a set of
    loaded domains as value.  If no domain could be loaded, it's value will be
    an empty set.
    """
    
    query = db.Query(DomainModel)
    
    if deleted is not None:
        query = query.filter("deleted ==", deleted)
    
    if disabled is not None:
        query = query.filter("disabled ==", disabled)
    
    query = query.filter("default ==", True)
    
    ids = dict.fromkeys(owner_ids)
    
    results = filter(lambda x:x.owner_id in owner_ids, map(None, query))
    
    for result in results:
        ids[result.owner_id] = _model_to_domain(result)
    
    return ids


def get_by_root_zone(connection, root_zone, deleted=None, disabled=None):
    """
    @summary: Gets a set of domains with the given root_zone.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type root_zone: Unicode
    @param root_zone: Root zone of the domain to be retrieved.
    
    @type deleted: Boolean
    @param deleted: Whether the Domain has been marked as deleted.  Pass None
    if you don't care if it is deleted or not.
    
    @type disabled: Boolean
    @param disabled: Whether the Domain is currently disabled.  Pass None if you
    don't care if it is disabled or not.
    
    @rtype: Set
    @return: Set of domains with the given root_zone.
    """
    
    query = db.Query(DomainModel).filter("root_zone ==", root_zone)
    
    if deleted is not None:
        query = query.filter("deleted ==", deleted)
    
    if disabled is not None:
        query = query.filter("disabled ==", disabled)
    
    results = set()
    
    for result in query:
        results.add(_model_to_domain(result=result))
    
    return results


def get_by_root_zones(connection, root_zones, deleted=None, disabled=None):
    """
    @summary: Gets a mapping of domains with given root zones.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type root_zones: Iterable
    @param root_zones: Iterable containing root zones of domains to be
    retrieved.
    
    @type deleted: Boolean
    @param deleted: Whether the Domain has been marked as deleted.  Pass None
    if you don't care if it is deleted or not.
    
    @type disabled: Boolean
    @param disabled: Whether the Domain is currently disabled.  Pass None if you
    don't care if it is disabled or not.
    
    @rtype: Dict
    @return: Dictionary with the root zone as key and a set of domains with
    the root zone as the value.
    """
    
    query = db.Query(DomainModel)
    
    if deleted is not None:
        query = query.filter("deleted ==", deleted)
    
    if disabled is not None:
        query = query.filter("disabled ==", disabled)
    
    ids = dict((root_zones[i], set()) for i in range(0, len(root_zones)))
    
    results = filter(lambda x:x.root_zone in root_zones, map(None, query))
    
    for result in results:
        ids[result.root_zone].add(_model_to_domain(result))
    
    return ids