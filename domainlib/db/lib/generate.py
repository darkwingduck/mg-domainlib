"""
Defines the library functions that are used to generate ids.
"""


from __future__ import unicode_literals
from idlib.friendly import generate
import logging

log = logging.getLogger(__name__)


def DEFAULT_GENERATE_INTERNAL_ID(length=10):
    """
    @summary: Generates an internal id for a domain.
    
    @type length: Integer
    @param length: Number of characters the internal id should be.
    
    @rtype: Unicode
    @return: Generated internal id of the given length.
    """
    return generate(length=length)


def DEFAULT_GENERATE_PUBLIC_ID(length=10):
    """
    @summary: Generates an internal id for a domain.
    
    @type length: Integer
    @param length: Number of characters the public id should be.
    
    @rtype: Unicode
    @return: Generated public id of the given length.
    """
    return generate(length=length)