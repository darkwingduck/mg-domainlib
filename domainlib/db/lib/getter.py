"""
Defines the library functions that will most likely be abstracted by each
database implementation.

Note: When unittesting, you can only achieve 97% with google app engine.
"""


from __future__ import unicode_literals
from domainlib import lib as dlib
from domainlib.db.lib.helpers import determine_db
from domainlib.db.lib.iterable import get_resolves_host_from_iterable
import logging

log = logging.getLogger(__name__)


def get_by_internal_id(connection, internal_id, deleted=None, disabled=None):
    """
    @summary: Gets a Domain from the database with the specified internal_id.
    
    This method is required by all implementations.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type internal_id: Unicode
    @param internal_id: Internal id of the domain to be retrieved.
    
    @type deleted: Boolean
    @param deleted: Whether the Domain has been marked as deleted.  Pass None
    if you don't care if it is deleted or not.
    
    @type disabled: Boolean
    @param disabled: Whether the Domain is currently disabled.  Pass None if you
    don't care if it is disabled or not.
    
    @rtype: domainlib.db.models.Domain
    @return: Loaded Domain model or None of not found.
    """
    
    impl = determine_db(connection=connection)
    
    if impl:
        return impl.get_by_internal_id(connection=connection,
                                       internal_id=internal_id,
                                       deleted=deleted,
                                       disabled=disabled)
    
    return None


def get_by_internal_ids(connection, internal_ids, deleted=None, disabled=None):
    """
    @summary: Gets Domains from the database with the specified internal_id.
    
    This method is required by all implementations.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type internal_ids: Iterable
    @param internal_ids: Iterable of internal ids of the domain to be retrieved.
    
    @type deleted: Boolean
    @param deleted: Whether the Domain has been marked as deleted.  Pass None
    if you don't care if it is deleted or not.
    
    @type disabled: Boolean
    @param disabled: Whether the Domain is currently disabled.  Pass None if you
    don't care if it is disabled or not.
    
    @rtype: Dict
    @return: Dictionary mapping of Domains with the internal id as key.  If no
    domain could be loaded, it's value will be None.
    """
    
    impl = determine_db(connection=connection)
    
    if impl:
        return impl.get_by_internal_ids(connection=connection,
                                       internal_ids=internal_ids,
                                       deleted=deleted,
                                       disabled=disabled)
    
    return dict.fromkeys(internal_ids)


def get_by_public_id(connection, public_id, deleted=None, disabled=None):
    """
    @summary: Gets a Domain from the database with the specified public_id.
    
    This method is required by all implementations.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type public_id: Unicode
    @param public_id: Public id of the domain to be retrieved.
    
    @type deleted: Boolean
    @param deleted: Whether the Domain has been marked as deleted.  Pass None
    if you don't care if it is deleted or not.
    
    @type disabled: Boolean
    @param disabled: Whether the Domain is currently disabled.  Pass None if you
    don't care if it is disabled or not.
    
    @rtype: domainlib.db.models.Domain
    @return: Loaded Domain model or None of not found.
    """
    
    impl = determine_db(connection=connection)
    
    if impl:
        return impl.get_by_public_id(connection=connection,
                                       public_id=public_id,
                                       deleted=deleted,
                                       disabled=disabled)
    
    return None


def get_by_public_ids(connection, public_ids, deleted=None, disabled=None):
    """
    @summary: Gets Domains from the database with the specified public_id.
    
    This method is required by all implementations.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type public_ids: Iterable
    @param public_ids: Iterable of public ids of the domain to be retrieved.
    
    @type deleted: Boolean
    @param deleted: Whether the Domain has been marked as deleted.  Pass None
    if you don't care if it is deleted or not.
    
    @type disabled: Boolean
    @param disabled: Whether the Domain is currently disabled.  Pass None if you
    don't care if it is disabled or not.
    
    @rtype: Dict
    @return: Dictionary mapping of Domains with the public id as key.  If no
    domain could be loaded, it's value will be None.
    """
    
    impl = determine_db(connection=connection)
    
    if impl:
        return impl.get_by_public_ids(connection=connection,
                                       public_ids=public_ids,
                                       deleted=deleted,
                                       disabled=disabled)
    
    return dict.fromkeys(public_ids)


def get_by_owner_id(connection, owner_id, deleted=None, disabled=None):
    """
    @summary: Gets a set of Domains from the database with the specified
    owner_id.
    
    This method is required by all implementations.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type owner_id: Unicode
    @param owner_id: Owner id of the domains to be retrieved.
    
    @type deleted: Boolean
    @param deleted: Whether the Domain has been marked as deleted.  Pass None
    if you don't care if it is deleted or not.
    
    @type disabled: Boolean
    @param disabled: Whether the Domain is currently disabled.  Pass None if you
    don't care if it is disabled or not.
    
    @rtype: Set
    @return: Set of Domains that have the specified owner id.
    """
    
    impl = determine_db(connection=connection)
    
    if impl:
        return impl.get_by_owner_id(connection=connection,
                                       owner_id=owner_id,
                                       deleted=deleted,
                                       disabled=disabled)
    
    return set()


def get_by_owner_ids(connection, owner_ids, deleted=None, disabled=None):
    """
    @summary: Gets Domains from the database with the specified owner_id.
    
    This method is required by all implementations.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type public_ids: Iterable
    @param public_ids: Iterable of public ids of the domain to be retrieved.
    
    @type deleted: Boolean
    @param deleted: Whether the Domain has been marked as deleted.  Pass None
    if you don't care if it is deleted or not.
    
    @type disabled: Boolean
    @param disabled: Whether the Domain is currently disabled.  Pass None if you
    don't care if it is disabled or not.
    
    @rtype: Dict
    @return: Dictionary mapping of Domains with the owner id as key and a set of
    loaded domains as value.  If no domain could be loaded, it's value will be
    an empty set.
    """
    
    impl = determine_db(connection=connection)
    
    if impl:
        return impl.get_by_owner_ids(connection=connection,
                                       owner_ids=owner_ids,
                                       deleted=deleted,
                                       disabled=disabled)
    
    return dict((owner_ids[i], set()) for i in range(0, len(owner_ids)))


def get_default_by_owner_id(connection, owner_id, deleted=None, disabled=None):
    """
    @summary: Gets the default Domain from the database with the specified
    owner_id.
    
    This method is required by all implementations.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type owner_id: Unicode
    @param owner_id: Owner id of the domains to be retrieved.
    
    @type deleted: Boolean
    @param deleted: Whether the Domain has been marked as deleted.  Pass None
    if you don't care if it is deleted or not.
    
    @type disabled: Boolean
    @param disabled: Whether the Domain is currently disabled.  Pass None if you
    don't care if it is disabled or not.
    
    @rtype: domainlib.db.models.Domain
    @return: Default Domains that has the specified owner id or None if no
    domain can be found.
    """
    
    impl = determine_db(connection=connection)
    
    if impl:
        return impl.get_default_by_owner_id(connection=connection,
                                            owner_id=owner_id,
                                            deleted=deleted,
                                            disabled=disabled)
    
    return set()


def get_default_by_owner_ids(connection, owner_ids, deleted=None, disabled=None):
    """
    @summary: Gets Domains from the database with the specified owner_id.
    
    This method is required by all implementations.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type public_ids: Iterable
    @param public_ids: Iterable of public ids of the domain to be retrieved.
    
    @type deleted: Boolean
    @param deleted: Whether the Domain has been marked as deleted.  Pass None
    if you don't care if it is deleted or not.
    
    @type disabled: Boolean
    @param disabled: Whether the Domain is currently disabled.  Pass None if you
    don't care if it is disabled or not.
    
    @rtype: Dict
    @return: Dictionary mapping of Domains with the owner id as key and domain
    as value.  If no domain could be loaded, it's value will be None
    """
    
    impl = determine_db(connection=connection)
    
    if impl:
        return impl.get_default_by_owner_ids(connection=connection,
                                             owner_ids=owner_ids,
                                             deleted=deleted,
                                             disabled=disabled)
    
    return dict.fromkeys(owner_ids)


def get_by_root_zone(connection, root_zone, deleted=None, disabled=None):
    """
    @summary: Gets a set of domains with the given root_zone.
    
    This method is required by all implementations.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type root_zone: Unicode
    @param root_zone: Root zone of the domain to be retrieved.
    
    @type deleted: Boolean
    @param deleted: Whether the Domain has been marked as deleted.  Pass None
    if you don't care if it is deleted or not.
    
    @type disabled: Boolean
    @param disabled: Whether the Domain is currently disabled.  Pass None if you
    don't care if it is disabled or not.
    
    @rtype: Set
    @return: Set of domains with the given root_zone.
    """
    
    impl = determine_db(connection=connection)
    
    if impl:
        return impl.get_by_root_zone(connection=connection,
                                            root_zone=root_zone,
                                            deleted=deleted,
                                            disabled=disabled)
    
    return set()


def get_by_root_zones(connection, root_zones, deleted=None, disabled=None):
    """
    @summary: Gets a mapping of domains with given root zones.
    
    This method is required by all implementations.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type root_zones: Iterable
    @param root_zones: Iterable containing root zones of domains to be
    retrieved.
    
    @type deleted: Boolean
    @param deleted: Whether the Domain has been marked as deleted.  Pass None
    if you don't care if it is deleted or not.
    
    @type disabled: Boolean
    @param disabled: Whether the Domain is currently disabled.  Pass None if you
    don't care if it is disabled or not.
    
    @rtype: Dict
    @return: Dictionary with the root zone as key and a set of domains with
    the root zone as the value.
    """
    
    impl = determine_db(connection=connection)
    
    if impl:
        return impl.get_by_root_zones(connection=connection,
                                      root_zones=root_zones,
                                      deleted=deleted,
                                      disabled=disabled)
    
    return dict((root_zones[i], set()) for i in range(0, len(root_zones)))


def get_resolves_host(connection, host, deleted=None, disabled=None):
    """
    @summary: Gets a domain that resolves a given host.
    
    This method is not required by any implementation.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type host: Unicode
    @param host: Unicode that contains a host name or some other uri that has
    a scheme, sub domain, and root zone.
    
    @type deleted: Boolean
    @param deleted: Whether the Domain has been marked as deleted.  Pass None
    if you don't care if it is deleted or not.
    
    @type disabled: Boolean
    @param disabled: Whether the Domain is currently disabled.  Pass None if you
    don't care if it is disabled or not.
    
    @rtype: Domain
    @return: Domain that successfully resolved the host, default domain if one
    is specified for the root zone of the host, or None if a default domain for
    the root zone cannot be found.
    """
    
    root_zone = dlib.split_domain(domain=host)[1]
    
    domains = get_by_root_zone(connection=connection,
                               root_zone=root_zone,
                               deleted=deleted,
                               disabled=disabled)
    
    return get_resolves_host_from_iterable(domains=domains,
                                           host=host,
                                           deleted=deleted,
                                           disabled=disabled,
                                           _root_zone=root_zone)