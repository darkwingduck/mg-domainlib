"""
Defines the library functions that are more utility/helper functions than
anything.
"""


from __future__ import unicode_literals
import logging

log = logging.getLogger(__name__)


def determine_db(connection):
    """
    @summary: Determines what database is being used and returns the correct
    implementation for that database.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @rtype: Module
    @return: Module for the specific database implementation or None if a not
    supported implementation.
    """
    
    try:
        from google.appengine.ext import db as gae_db
        
        if connection == gae_db:
            from domainlib.db.impl.gae import lib as gae_lib
            return gae_lib
        
        else:
            raise Exception("Not Google App Engine")
    
    except:
        pass
    
    return None