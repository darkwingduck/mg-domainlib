"""
Defines the library functions that do not require the database for use.
"""


from __future__ import unicode_literals
from domainlib import lib as dlib
import logging

log = logging.getLogger(__name__)


def get_by_internal_id_from_iterable(domains, internal_id, deleted=None,
                                     disabled=None):
    """
    @summary: Gets a domain in a iterable that has a matching internal_id.
    
    @type domains: Iterable
    @param domains: Iterable containing domainlib.db.models.Domain instances.
    
    @type internal_id: Unicode
    @param internal_id: Internal id of the domain to be retrieved.
    
    @type deleted: Boolean
    @param deleted: Whether the Domain has been marked as deleted.  Pass None
    if you don't care if it is deleted or not.
    
    @type disabled: Boolean
    @param disabled: Whether the Domain is currently disabled.  Pass None if you
    don't care if it is disabled or not.
    
    @rtype: domainlib.db.models.Domain or None
    @return: Domain instance with matching internal_id or None if no domain
    could be found.
    """
    
    if deleted is not None:
        domains = filter(lambda x:True if x.deleted == deleted else False,
                         domains)
    
    if disabled is not None:
        domains = filter(lambda x:True if x.disabled == disabled else False,
                         domains)
    
    for domain in domains:
        if domain.internal_id == internal_id:
            return domain
    
    return None


def get_by_internal_ids_from_iterable(domains, internal_ids, deleted=None,
                                      disabled=None):
    """
    @summary: Gets all domains in a iterable that have an internal_id in
    internal_ids.
    
    @type domains: Iterable
    @param domains: Iterable containing domainlib.db.models.Domain instances.
    
    @type internal_ids: Iterable
    @param internal_ids: Iterable of internal ids of the domains to be
    retrieved.
    
    @type deleted: Boolean
    @param deleted: Whether the Domain has been marked as deleted.  Pass None
    if you don't care if it is deleted or not.
    
    @type disabled: Boolean
    @param disabled: Whether the Domain is currently disabled.  Pass None if you
    don't care if it is disabled or not.
    
    @rtype: Dict
    @return: Dictionary mapping of domains with internal_id being key and
    domain being value.  If no domain can be found for a internal_id, it's
    value will be None.
    """
    
    found = dict.fromkeys(internal_ids)
    
    if deleted is not None:
        domains = filter(lambda x:True if x.deleted == deleted else False,
                         domains)
    
    if disabled is not None:
        domains = filter(lambda x:True if x.disabled == disabled else False,
                         domains)
    
    for domain in domains:
        if domain.internal_id in internal_ids:
            found[domain.internal_id] = domain
    
    return found


def get_by_public_id_from_iterable(domains, public_id, deleted=None,
                                      disabled=None):
    """
    @summary: Gets a domain in a iterable that has a matching public_id.
    
    @type domains: Iterable
    @param domains: Iterable containing domainlib.db.models.Domain instances.
    
    @type public_id: Unicode
    @param public_id: Public id of the domain to be retrieved.
    
    @type deleted: Boolean
    @param deleted: Whether the Domain has been marked as deleted.  Pass None
    if you don't care if it is deleted or not.
    
    @type disabled: Boolean
    @param disabled: Whether the Domain is currently disabled.  Pass None if you
    don't care if it is disabled or not.
    
    @rtype: domainlib.db.models.Domain or None
    @return: Domain instance with matching public_id or None if no domain
    could be found.
    """
    
    if deleted is not None:
        domains = filter(lambda x:True if x.deleted == deleted else False,
                         domains)
    
    if disabled is not None:
        domains = filter(lambda x:True if x.disabled == disabled else False,
                         domains)
    
    for domain in domains:
        if domain.public_id == public_id:
            return domain
    
    return None


def get_by_public_ids_from_iterable(domains, public_ids, deleted=None,
                                      disabled=None):
    """
    @summary: Gets all domains in a iterable that have an public_id in
    public_ids.
    
    @type domains: Iterable
    @param domains: Iterable containing domainlib.db.models.Domain instances.
    
    @type public_ids: Iterable
    @param public_ids: Iterable of public ids of the domains to be
    retrieved.
    
    @type deleted: Boolean
    @param deleted: Whether the Domain has been marked as deleted.  Pass None
    if you don't care if it is deleted or not.
    
    @type disabled: Boolean
    @param disabled: Whether the Domain is currently disabled.  Pass None if you
    don't care if it is disabled or not.
    
    @rtype: Dict
    @return: Dictionary mapping of domains with public_id being key and
    domain being value.  If no domain can be found for a public_id, it's
    value will be None.
    """
    
    found = dict.fromkeys(public_ids)
    
    if deleted is not None:
        domains = filter(lambda x:True if x.deleted == deleted else False,
                         domains)
    
    if disabled is not None:
        domains = filter(lambda x:True if x.disabled == disabled else False,
                         domains)
    
    for domain in domains:
        if domain.public_id in public_ids:
            found[domain.public_id] = domain
    
    return found


def get_by_owner_id_from_iterable(domains, owner_id, deleted=None,
                                      disabled=None):
    """
    @summary: Gets all domains in a iterable that has a matching owner_id.
    
    @type domains: Iterable
    @param domains: Iterable containing domainlib.db.models.Domain instances.
    
    @type public_id: Unicode
    @param public_id: Owner id of the domain to be retrieved.
    
    @type deleted: Boolean
    @param deleted: Whether the Domain has been marked as deleted.  Pass None
    if you don't care if it is deleted or not.
    
    @type disabled: Boolean
    @param disabled: Whether the Domain is currently disabled.  Pass None if you
    don't care if it is disabled or not.
    
    @rtype: Set
    @return: Set of Domains that have a matching owner_id
    """
    
    found = set()
    
    if deleted is not None:
        domains = filter(lambda x:True if x.deleted == deleted else False,
                         domains)
    
    if disabled is not None:
        domains = filter(lambda x:True if x.disabled == disabled else False,
                         domains)
    
    for domain in domains:
        if domain.owner_id == owner_id:
            found.add(domain)
    
    return found


def get_by_owner_ids_from_iterable(domains, owner_ids, deleted=None,
                                      disabled=None):
    """
    @summary: Gets all domains in a iterable that have an owner_id in
    owner_ids.
    
    @type domains: Iterable
    @param domains: Iterable containing domainlib.db.models.Domain instances.
    
    @type owner_ids: Iterable
    @param owner_ids: Iterable of owner ids of the domains to be
    retrieved.
    
    @type deleted: Boolean
    @param deleted: Whether the Domain has been marked as deleted.  Pass None
    if you don't care if it is deleted or not.
    
    @type disabled: Boolean
    @param disabled: Whether the Domain is currently disabled.  Pass None if you
    don't care if it is disabled or not.
    
    @rtype: Dict
    @return: Dictionary mapping of domains with owner_id being key and
    set of found domains being value.
    """
    
    found = dict((owner_ids[i], set()) for i in range(0, len(owner_ids)))
    
    if deleted is not None:
        domains = filter(lambda x:True if x.deleted == deleted else False,
                         domains)
    
    if disabled is not None:
        domains = filter(lambda x:True if x.disabled == disabled else False,
                         domains)
    
    for domain in domains:
        if domain.owner_id in owner_ids:
            found[domain.owner_id].add(domain)
    
    return found


def get_default_by_owner_id_from_iterable(domains, owner_id, deleted=None,
                                      disabled=None):
    """
    @summary: Gets the default domain in a iterable that has a matching
    owner_id.
    
    @type domains: Iterable
    @param domains: Iterable containing domainlib.db.models.Domain instances.
    
    @type public_id: Unicode
    @param public_id: Owner id of the domain to be retrieved.
    
    @type deleted: Boolean
    @param deleted: Whether the Domain has been marked as deleted.  Pass None
    if you don't care if it is deleted or not.
    
    @type disabled: Boolean
    @param disabled: Whether the Domain is currently disabled.  Pass None if you
    don't care if it is disabled or not.
    
    @rtype: domainlib.db.models.Domain or None
    @return: Domain instance with matching owner_id and set to be the default or
    None if no domain could be found.
    """
    
    if deleted is not None:
        domains = filter(lambda x:True if x.deleted == deleted else False,
                         domains)
    
    if disabled is not None:
        domains = filter(lambda x:True if x.disabled == disabled else False,
                         domains)
    
    for domain in domains:
        if domain.owner_id == owner_id and domain.default:
            return domain
    
    return None


def get_default_by_owner_ids_from_iterable(domains, owner_ids, deleted=None,
                                      disabled=None):
    """
    @summary: Gets all default domains in a iterable that have an owner_id in
    owner_ids.
    
    @type domains: Iterable
    @param domains: Iterable containing domainlib.db.models.Domain instances.
    
    @type owner_ids: Iterable
    @param owner_ids: Iterable of owner ids of the domains to be
    retrieved.
    
    @type deleted: Boolean
    @param deleted: Whether the Domain has been marked as deleted.  Pass None
    if you don't care if it is deleted or not.
    
    @type disabled: Boolean
    @param disabled: Whether the Domain is currently disabled.  Pass None if you
    don't care if it is disabled or not.
    
    @rtype: Dict
    @return: Dictionary mapping of defaults with owner_id being key and default
    domain being value.  If no domain can be found for a owner_id, it's
    value will be None.
    """
    
    defaults = dict.fromkeys(owner_ids)
    
    if deleted is not None:
        domains = filter(lambda x:True if x.deleted == deleted else False,
                         domains)
    
    if disabled is not None:
        domains = filter(lambda x:True if x.disabled == disabled else False,
                         domains)
    
    for domain in domains:
        if domain.owner_id in owner_ids and domain.default:
            defaults[domain.owner_id] = domain
    
    return defaults


def get_resolves_host_from_iterable(domains, host, deleted=None, disabled=None,
                                    _root_zone=None):
    """
    @summary: Gets a domain that resolves a given host.
    
    @type domains: Iterable
    @param domains: Iterable containing domainlib.db.models.Domain instances.
    
    @type host: Unicode
    @param host: Unicode that contains a host name or some other uri that has
    a scheme, sub domain, and root zone.
    
    @type deleted: Boolean
    @param deleted: Whether the Domain has been marked as deleted.  Pass None
    if you don't care if it is deleted or not.
    
    @type disabled: Boolean
    @param disabled: Whether the Domain is currently disabled.  Pass None if you
    don't care if it is disabled or not.
    
    @type _root_zone: Unicode
    @param _root_zone: Parsed root zone of the host.  If it has already been
    parsed, no need to do it again
    
    @rtype: Domain
    @return: Domain that successfully resolved the host, default domain if one
    is specified for the root zone of the host, or None if a default domain for
    the root zone cannot be found.
    """
    
    if not _root_zone:
        _root_zone = dlib.split_domain(domain=host)[1]
    
    if deleted is not None:
        domains = filter(lambda x:True if x.deleted == deleted else False,
                         domains)
    
    if disabled is not None:
        domains = filter(lambda x:True if x.disabled == disabled else False,
                         domains)
    
    domains = filter(lambda x:True if x.root_zone == _root_zone else False,
                     domains)
    
    #getting the default domain for the root zone
    default = None
    
    for domain in domains:
        if domain.default:
            default = domain
    
    #we aren't looking at the database models, but the regular domain model
    regex_domains = map(lambda x:x.domain, domains)
    resolved_regex_domain = dlib.find_domain(domain=host,
                                             domains=regex_domains,
                                             default=None)
    
    resolved_domain = None
    
    for domain in domains:
        if domain.domain == resolved_regex_domain:
            resolved_domain = domain
    
    return resolved_domain or default