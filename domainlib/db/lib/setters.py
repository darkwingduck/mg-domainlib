"""
Defines the library functions that are used to set information
"""


from __future__ import unicode_literals
from domainlib.db.lib.generate import (DEFAULT_GENERATE_INTERNAL_ID,
     DEFAULT_GENERATE_PUBLIC_ID)

from domainlib.db.lib.validate import (DEFAULT_VALIDATE_INTERNAL_ID,
     DEFAULT_VALIDATE_PUBLIC_ID)

from domainlib.db.lib.helpers import determine_db
from domainlib.db.lib.getter import get_by_root_zone
import logging

log = logging.getLogger(__name__)


def update(connection, domain,
           generate_internal_id=DEFAULT_GENERATE_INTERNAL_ID,
           validate_internal_id=DEFAULT_VALIDATE_INTERNAL_ID,
           generate_public_id=DEFAULT_GENERATE_PUBLIC_ID,
           validate_public_id=DEFAULT_VALIDATE_PUBLIC_ID,
           _fail_first_generated_ids=False):
    
    """
    @summary: Updates a Domain instance to the connected database.  The instance
    must exists before updating.
    
    This method is required by all implementations.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type domain: domainlib.db.models.Domain
    @param domain: Domain instance being saved to the database.  If the domain
    does not have a _key, it will be created.
    
    If the domain was created, it's internal_id and public_id will be created
    if they were not previously specified.
    
    @type generate_internal_id: Function
    @param generate_internal_id: Function used to generate a unique internal
    id for created domains.  This function takes no arguments and should return
    a unicode of 500 characters or less.
    
    This function will only be called if the internal_id has not been set yet.
    
    @type validate_internal_id: Function
    @param validate_internal_id: Function used to validate that a generated
    internal id is unique.  This function should take a connection and unicode
    and return True if the id is unique and False if it is not unique.
    
    This function will only be called if the internal_id has not been set yet.
    
    @type generate_public_id: Function
    @param generate_public_id: Function used to generate a unique public
    id for created domains.  This function takes no arguments and should return
    a unicode of 500 characters or less.
    
    This function will only be called if the public_id has not been set yet.
    
    @type validate_public_id: Function
    @param validate_public_id: Function used to validate that a generated
    public id is unique.  This function should take a connection and unicode
    and return True if the id is unique and False if it is not unique.
    
    This function will only be called if the public_id has not been set yet.
    
    @type _fail_first_generated_ids: Boolean
    @param _fail_first_generated_ids: Whether to fail the first generated
    internal and public ids.  This is only used by unittests.
    
    @rtype: Boolean
    @return: Whether the instance could be saved or not.
    """
    
    impl = determine_db(connection=connection)
    
    if impl:
        return impl.update(connection=connection,
                         domain=domain,
                         generate_internal_id=generate_internal_id,
                         validate_internal_id=validate_internal_id,
                         generate_public_id=generate_public_id,
                         validate_public_id=validate_public_id,
                         _fail_first_generated_ids=_fail_first_generated_ids)
    
    return False



    
    
def set_default_for_root_zone(connection, domain,
                              generate_internal_id=DEFAULT_GENERATE_INTERNAL_ID,
                              validate_internal_id=DEFAULT_VALIDATE_INTERNAL_ID,
                              generate_public_id=DEFAULT_GENERATE_PUBLIC_ID,
                              validate_public_id=DEFAULT_VALIDATE_PUBLIC_ID,
                              _fail_implementation=False,
                              _raise_implementation_exception=False,
                              _fail_non_impl_root_zone_set=False):
    
    """
    @summary: Sets the given domain as the default for it's root zone.
    
    The domain will only be set as default if it is not disabled and not
    deleted.
    
    If an error occurs and the underlying implentation does not support
    transactions, data corruption may occur.
    
    This method is optional to implementations.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type domain: domainlib.db.models.Domain
    @param domain: Domain instance being set as default.
    
    @type generate_internal_id: Function
    @param generate_internal_id: Function used to generate a unique internal
    id for created domains.  This function takes no arguments and should return
    a unicode of 500 characters or less.
    
    This function will only be called if the internal_id has not been set yet.
    
    @type validate_internal_id: Function
    @param validate_internal_id: Function used to validate that a generated
    internal id is unique.  This function should take a connection and unicode
    and return True if the id is unique and False if it is not unique.
    
    This function will only be called if the internal_id has not been set yet.
    
    @type generate_public_id: Function
    @param generate_public_id: Function used to generate a unique public
    id for created domains.  This function takes no arguments and should return
    a unicode of 500 characters or less.
    
    This function will only be called if the public_id has not been set yet.
    
    @type validate_public_id: Function
    @param validate_public_id: Function used to validate that a generated
    public id is unique.  This function should take a connection and unicode
    and return True if the id is unique and False if it is not unique.
    
    This function will only be called if the public_id has not been set yet.
    
    @type _fail_implementation: Boolean
    @param _fail_implementation: Don't use the database implementation if it
    available.  This is only used in unittesting.
    
    @type _raise_implementation_exception: Boolean
    @param _raise_implementation_exception: Whether to raise an exception to
    check the implementation failure.
    
    @type _fail_non_impl_root_zone_set: Boolean
    @param _fail_non_impl_root_zone_set: Whether to fail setting defaults to
    False for existing root zones when the db doesn't implement its own method.
    This is only used in unittesting.
    
    @rtype: Boolean
    @return: Whether the domain could be set as default or not.
    """
    
    if not domain.deleted and not domain.disabled:
        if not _fail_implementation:
            impl = determine_db(connection=connection)
        
        else:
            impl = None
        
        #checking if the database implementation has a method to perform
        #this update in a single transaction
        if impl and hasattr(impl, "set_default_for_root_zone"):
            try:
                if _raise_implementation_exception:
                    raise Exception("Failing implementation for unittest")
                
                return impl.set_default_for_root_zone(connection=connection,
                                      domain=domain,
                                      generate_internal_id=generate_internal_id,
                                      validate_internal_id=validate_internal_id,
                                      generate_public_id=generate_public_id,
                                      validate_public_id=validate_public_id)
            
            except Exception as e:
                log.error(("Could not set domain as default for "
                           "root_zone:'%s', internal_id:'%s', error:'%s'"
                           %(domain.root_zone,domain.internal_id,e)))
                
                return False
        
        else:
            all_domains_for_root_zone = get_by_root_zone(
                                                 connection=connection,
                                                 root_zone=domain.root_zone,
                                                 deleted=None,
                                                 disabled=None)
            
            all_domains_for_root_zone = filter(lambda x:x != domain,
                                               all_domains_for_root_zone)
            
            for dom in all_domains_for_root_zone:
                dom.default = False
                valid = update(connection=connection,
                               domain=dom,
                               generate_internal_id=generate_internal_id,
                               validate_internal_id=validate_internal_id,
                               generate_public_id=generate_public_id,
                               validate_public_id=validate_public_id)
                
                if _fail_non_impl_root_zone_set or not valid:
                    return False
            
            domain.default = True
            return update(connection=connection, domain=domain,
                          generate_internal_id=generate_internal_id,
                          validate_internal_id=validate_internal_id,
                          generate_public_id=generate_public_id,
                          validate_public_id=validate_public_id)
    
    else:
        return False


def _set_property_and_new_default(connection, prop, value, domain,
            new_default=None,
            generate_internal_id=DEFAULT_GENERATE_INTERNAL_ID,
            validate_internal_id=DEFAULT_VALIDATE_INTERNAL_ID,
            generate_public_id=DEFAULT_GENERATE_PUBLIC_ID,
            validate_public_id=DEFAULT_VALIDATE_PUBLIC_ID,
            _fail_default_root_zone=False):
    
    """
    @summary: Sets a property on the given domain to the given value.
    
    If the domain is default, checks that the provided new_default's root zone
    matches this domain and it is not disabled and not deleted.

    This method is not required by any implementation.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type prop: Unicode
    @param prop: Name of the property being set.
    
    @type value: Object
    @param value: Value of the property being set.
    
    @type domain: domainlib.db.models.Domain
    @param domain: Domain instance the property is being set on.
    
    @type new_default: Domain
    @param new_default: Domain to be set as default if the domain with the
    property being set is default.
    
    @type generate_internal_id: Function
    @param generate_internal_id: Function used to generate a unique internal
    id for created domains.  This function takes no arguments and should return
    a unicode of 500 characters or less.
    
    This function will only be called if the internal_id has not been set yet.
    
    @type validate_internal_id: Function
    @param validate_internal_id: Function used to validate that a generated
    internal id is unique.  This function should take a connection and unicode
    and return True if the id is unique and False if it is not unique.
    
    This function will only be called if the internal_id has not been set yet.
    
    @type generate_public_id: Function
    @param generate_public_id: Function used to generate a unique public
    id for created domains.  This function takes no arguments and should return
    a unicode of 500 characters or less.
    
    This function will only be called if the public_id has not been set yet.
    
    @type validate_public_id: Function
    @param validate_public_id: Function used to validate that a generated
    public id is unique.  This function should take a connection and unicode
    and return True if the id is unique and False if it is not unique.
    
    This function will only be called if the public_id has not been set yet.
    
    @type _fail_default_root_zone: Boolean
    @param _fail_default_root_zone: Whether to fail setting the default root
    zone.  This is used for unittesting.
    
    @rtype: Boolean
    @return: Whether the property could be set or not.
    """
    
    #these will be passed into methods that call update, I just hate seeing
    #all of this being passed over and over again.  The passing is neccessary
    #but there is no reason to cluter the code.
    update_optional_args = {
        "generate_internal_id" : generate_internal_id,
        "validate_internal_id" : validate_internal_id,
        "generate_public_id" : generate_public_id,
        "validate_public_id" : validate_public_id
    }
    
    #this will hold the original value of the property in case we need to
    #reset it due to a failed operation.
    original_value = getattr(domain, prop, None)
    
    if domain.default:
        if (not new_default or new_default.root_zone != domain.root_zone
            or new_default.disabled or new_default.deleted):
            
            return False
        
        else:
            domain.default = False
            setattr(domain, prop, value)
            
            if update(connection=connection,
                      domain=domain,
                      **update_optional_args):
                
                if (not _fail_default_root_zone
                    and set_default_for_root_zone(connection=connection,
                     domain=new_default,
                     **update_optional_args)):
                
                    return True
                
                else:
                    #we couldn't set the new_default as default so revert the
                    #changes on this domain
                    domain.default = True
                    setattr(domain, prop, original_value)
                    
                    update(connection=connection,
                           domain=domain,
                           **update_optional_args)
            
            return False
    
    else:
        domain.disabled = True
        
        return update(connection=connection,
                      domain=domain,
                      **update_optional_args)


def disable(connection, domain, new_default=None,
            generate_internal_id=DEFAULT_GENERATE_INTERNAL_ID,
            validate_internal_id=DEFAULT_VALIDATE_INTERNAL_ID,
            generate_public_id=DEFAULT_GENERATE_PUBLIC_ID,
            validate_public_id=DEFAULT_VALIDATE_PUBLIC_ID,
            _fail_default_root_zone=False):
    
    """
    @summary: Disables a domain for its root zone.
    
    If the domain is default, checks that the provided new_default's root zone
    matches this domain and it is not disabled and not deleted.

    This method is not required by any implementation.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type domain: domainlib.db.models.Domain
    @param domain: Domain instance being disabled.
    
    @type new_default: Domain
    @param new_default: Domain to be set as default if the domain being disabled
    is the current default.
    
    @type generate_internal_id: Function
    @param generate_internal_id: Function used to generate a unique internal
    id for created domains.  This function takes no arguments and should return
    a unicode of 500 characters or less.
    
    This function will only be called if the internal_id has not been set yet.
    
    @type validate_internal_id: Function
    @param validate_internal_id: Function used to validate that a generated
    internal id is unique.  This function should take a connection and unicode
    and return True if the id is unique and False if it is not unique.
    
    This function will only be called if the internal_id has not been set yet.
    
    @type generate_public_id: Function
    @param generate_public_id: Function used to generate a unique public
    id for created domains.  This function takes no arguments and should return
    a unicode of 500 characters or less.
    
    This function will only be called if the public_id has not been set yet.
    
    @type validate_public_id: Function
    @param validate_public_id: Function used to validate that a generated
    public id is unique.  This function should take a connection and unicode
    and return True if the id is unique and False if it is not unique.
    
    This function will only be called if the public_id has not been set yet.
    
    @type _fail_default_root_zone: Boolean
    @param _fail_default_root_zone: Whether to fail setting the default root
    zone.  This is used for unittesting.
    
    @rtype: Boolean
    @return: Whether the domain could be disabled or not.
    """
    
    return _set_property_and_new_default(connection=connection,
                             prop="disabled",
                             value=True,
                             domain=domain,
                             new_default=new_default,
                             generate_internal_id=generate_internal_id,
                             validate_internal_id=validate_internal_id,
                             generate_public_id=generate_public_id,
                             validate_public_id=validate_public_id,
                             _fail_default_root_zone=_fail_default_root_zone)


def delete(connection, domain, new_default=None,
           generate_internal_id=DEFAULT_GENERATE_INTERNAL_ID,
           validate_internal_id=DEFAULT_VALIDATE_INTERNAL_ID,
           generate_public_id=DEFAULT_GENERATE_PUBLIC_ID,
           validate_public_id=DEFAULT_VALIDATE_PUBLIC_ID,
           _fail_default_root_zone=False):
    
    """
    @summary: Deletes a domain for its root zone.
    
    If the domain is default, checks that the provided new_default's root zone
    matches this domain and it is not disabled and not deleted.
    
    This method is not required by any implementation.
    
    @type connection: Object
    @param connection: Connection to a database used to identify what database
    implementation is being used.
    
    @type domain: domainlib.db.models.Domain
    @param domain: Domain instance being deleted.
    
    @type new_default: Domain
    @param new_default: Domain to be set as default if the domain being disabled
    is the current default.
    
    @type generate_internal_id: Function
    @param generate_internal_id: Function used to generate a unique internal
    id for created domains.  This function takes no arguments and should return
    a unicode of 500 characters or less.
    
    This function will only be called if the internal_id has not been set yet.
    
    @type validate_internal_id: Function
    @param validate_internal_id: Function used to validate that a generated
    internal id is unique.  This function should take a connection and unicode
    and return True if the id is unique and False if it is not unique.
    
    This function will only be called if the internal_id has not been set yet.
    
    @type generate_public_id: Function
    @param generate_public_id: Function used to generate a unique public
    id for created domains.  This function takes no arguments and should return
    a unicode of 500 characters or less.
    
    This function will only be called if the public_id has not been set yet.
    
    @type validate_public_id: Function
    @param validate_public_id: Function used to validate that a generated
    public id is unique.  This function should take a connection and unicode
    and return True if the id is unique and False if it is not unique.
    
    This function will only be called if the public_id has not been set yet.
    
    @type _fail_default_root_zone: Boolean
    @param _fail_default_root_zone: Whether to fail setting the default root
    zone.  This is used for unittesting.
    
    @rtype: Boolean
    @return: Whether the domain could be deleted or not.
    """
    
    return _set_property_and_new_default(connection=connection,
                             prop="deleted",
                             value=True,
                             domain=domain,
                             new_default=new_default,
                             generate_internal_id=generate_internal_id,
                             validate_internal_id=validate_internal_id,
                             generate_public_id=generate_public_id,
                             validate_public_id=validate_public_id,
                             _fail_default_root_zone=_fail_default_root_zone)