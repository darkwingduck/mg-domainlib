"""
Defines the base models that database implementations models must store data
for.
"""


from __future__ import unicode_literals
from domainlib.domain import Domain as _Domain


class Domain(object):
    """
    @summary: Defines the database independent Domain model that each database
    will load data into and save data from.
    
    @type _key: Object
    @ivar _key: Database specific object or unique identifier for the domain.
    
    @type internal_id: Unicode
    @ivar internal_id: ID used internally by the system to identify this domain.
    
    This should never be an auto incrementing field to reduce pain during
    migrations.
    
    This field should be indexed.
    
    @type public_id: Unicode
    @ivar public_id: ID used when interacting with the domain through a publicly
    accessible api.
    
    This field should be indexed.
    
    @type owner_id: Unicode
    @ivar owner_id: Internal id of an object that owns this domain.  Most likely
    some sort of site or instance id.
    
    This field should be indexed.
    
    @type root_zone: Unicode
    @ivar root_zone: Non-regex root zone of the domain.  This will be used when
    looking up a domain that could match the HTTP_HOST.
    
    This field should be indexed.
    
    @type default: Boolean
    @ivar default: When this domain is in an iterable of domains to be searched,
    use this as default if a match cannot be found.
    
    This field should be indexed.
    
    @type domain: domainlib.domain.Domain
    @ivar domain: Instance of domainlib.domain.Domain that can be used by
    domainlib.lib.find_domain.
    
    @type created: Datetime
    @ivar created: Datetime when the model was created in the database.
    
    This field should be indexed.
    
    @type updated: Datetime
    @ivar updated: Datetime the model was last updated in the database.
    
    This field should be indexed.
    
    @type disabled: Boolean
    @ivar disabled: Whether this domain should not be used when matching.
    
    When disabling a domain, use domainlib.db.lib.disable instead of setting
    this directly to account for root zone defaults.
    
    This field should be indexed.
    
    @type deleted: Boolean
    @ivar deleted: Whether this domain has been marked to never be used again.
    
    When deleting a domain, use domainlib.db.lib.delete instead of setting
    this directly to account for root zone defaults.
    
    This field should be indexed.
    """
    
    
    def __init__(self):
        self._key = None
        self.internal_id = ""
        self.public_id = ""
        self.owner_id = ""
        self.root_zone = ""
        self.default = False
        self.domain = _Domain()
        self.created = None
        self.updated = None
        self.disabled = False
        self.deleted = False
    
    
    def __eq__(self, other):
        """
        @summary: Checks if the two domains internal ids are the same
        """
        
        return other.internal_id == self.internal_id
    
    
    def __ne__(self, other):
        """
        @summary: Checks if the two domains internal ids are not the same
        """
        
        return other.internal_id != self.internal_id