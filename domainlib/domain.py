"""
Defines the data model for domains.
"""


from __future__ import unicode_literals


class Domain(object):
    """
    @summary: Defines the data structure for domains.
    
    There are three regex for the parts of a domain rather than one large regex
    to try and simplify the writing of each parts regex.
    
    @type root_zone: StringLiteral
    @ivar root_zone: Regular expression used to match the root zone.
    
    @type sub_domain: StringLiteral
    @ivar sub_domain: Regular expression used to match the sub domain.
    
    This pattern must not include the scheme, root zone, or port.  When finding
    a domain, the scheme, root zone, and port will automatically be separated
    and checked against separately.
    
    @type port:StringLiteral
    @ivar port:Regular expression used to match the port of the site.
    
    @type weight: Float
    @ivar weight: Weighting used to give this domain precedence over another
    matching domain.
    """
    
    
    def __init__(self, root_zone="", sub_domain="",port=r"(^[0-9]+|^)$",
                 weight=0.0):
        
        self.root_zone = root_zone
        self.sub_domain = sub_domain
        self.port = port
        self.weight = weight