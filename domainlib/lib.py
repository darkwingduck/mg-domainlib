"""
Defines non-db library functions
"""


from __future__ import unicode_literals
import re


def split_domain(domain):
    """
    @summary: Splits a domain into its subdomain, root zone, and port
    
    @type domain: Unicode
    @param domain: Unicode domain most likely from a HTTP_HOST header that is
    being split
    
    @rtype: Tuple
    @return: Tuple containing the subdomain, root zone, and port in lowercase.
    All parts returned will be unicode.
    """
    
    #removing the scheme if present
    if "://" in domain:
        domain = domain[(domain.find("://") + 3):]
    
    #removing any paths if present
    if "/" in domain:
        domain = domain[:domain.find("/")]
    
    #removing the port if present
    port = ""
    if ":" in domain:
        port = domain[domain.find(":")+1:]
        domain = domain[:domain.find(":")]
    
    domain_parts = domain.split(".")
    root_zone = ".".join(domain_parts[-2:])
    
    domain_parts = domain_parts[:-2]
    sub_domain = ".".join(domain_parts)
    
    return sub_domain, root_zone, port


def find_domain(domain, domains, default=None):
    """
    @summary: Finds a Domain instance who's root zone and subdomains match the
    given domain.
    
    @type domain: Unicode
    @param domain: Unicode domain most likely from a HTTP_HOST header that is
    being searched for.
    
    @type domains: Iterable
    @param domains: Iterable of Domain instances being searched through to find
    a match.
    
    @type default: Domain
    @param default: Domain returned if a match could not be found.
    """
    
    sub_domain, root_zone, port = split_domain(domain=domain)
    
    matching = []
    
    for dom in domains:
        if re.match(dom.root_zone, root_zone, re.IGNORECASE):
            if port:
                if (re.match(dom.port, port, re.IGNORECASE)
                    and re.match(dom.sub_domain, sub_domain, re.IGNORECASE)):
                    
                    matching.append(dom)
            
            else:
                if re.match(dom.sub_domain, sub_domain, re.IGNORECASE):
                    matching.append(dom)
    
    matching.sort(key=lambda x:x.weight)
    
    if matching:
        return matching.pop()
    
    else:
        return default