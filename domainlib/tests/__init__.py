"""
Imports of all the unittest that should be run.
"""

import os, unittest, argparse, sys, inspect

PACKAGE_PATH = os.path.dirname(os.path.abspath(
                       inspect.getframeinfo(inspect.currentframe()).filename))

sys.path.append(os.path.join(PACKAGE_PATH, "../../"))

#do not import the dependent test case, it is supposed to be a base class for
#db unittest
from domainlib.tests.lib import LibTestCase
from domainlib.tests.db.independent import IndependentTestCase

#parsing the args because I want to know what test to load without having to
#do a bunch of tricks and define multiple test suites
parser = argparse.ArgumentParser()
parser.add_argument("--gae", dest="gae", action="store_false")
args = parser.parse_args()

#loading the google app engine test
if args.gae:
    from domainlib.tests.db.impl.gae.lib import GAETestCase


def main():
    #runs the default discoverable test
    unittest.main()
    
    
if __name__ == "__main__":
    main()