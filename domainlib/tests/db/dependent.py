"""
Defines unit test for use with databases
"""


from __future__ import unicode_literals
import unittest
from domainlib.db.models import Domain
from domainlib.db import lib


class DependentTestCase(unittest.TestCase):
    """
    @summary: Runs database specific test
    """
    
    
    def setUp(self):
        #database specific instances must define this object
        self.db = None
        
    
    def postSetUp(self):
        """
        @summary: Defines the actual setup of the testcase that isn't self.db
        specific.
        """
        
        testDomain = Domain()
        testDomain.root_zone = "example.com"
        testDomain.internal_id = "internal"
        testDomain.public_id = "public"
        testDomain.owner_id = "donny"
        testDomain.domain.root_zone = r"example\.com"
        lib.update(connection=self.db, domain=testDomain)
    
    
    def test_create(self):
        """
        @summary: Test creating a domain object
        """
        
        testDomain = Domain()
        testDomain.root_zone = "example.com"
        testDomain.owner_id = "donny"
        testDomain.domain.root_zone = r"example\.com"
        
        self.assertTrue(lib.update(connection=self.db,
                                   domain=testDomain,
                                   _fail_first_generated_ids=True))
        
        testDomain2 = Domain()
        testDomain2.root_zone = "example.com"
        testDomain2.owner_id = "donny"
        testDomain2.domain.root_zone = r"example\.com"
        
        self.assertFalse(lib.update(connection=self.db,
                                   domain=testDomain2,
                                   generate_internal_id=None))
    
    def test_get_by_internal_id(self):
        """
        @summary: Test getting a domain by its internal id.
        """
        
        domain = lib.get_by_internal_id(connection=self.db,
                                        internal_id="internal",
                                        deleted=None,
                                        disabled=None)
        
        self.assertIsInstance(domain, Domain)
        self.assertEqual(domain.public_id, "public")
        
        domain.deleted = True
        lib.update(connection=self.db, domain=domain)
        
        domain = lib.get_by_internal_id(connection=self.db,
                                        internal_id="internal",
                                        deleted=False,
                                        disabled=None)
        
        self.assertIsNone(domain)
        
        domain = lib.get_by_internal_id(connection=self.db,
                                        internal_id="internal",
                                        deleted=True,
                                        disabled=None)
        
        self.assertIsInstance(domain, Domain)
        self.assertEqual(domain.public_id, "public")
        
        domain.deleted = False
        domain.disabled = True
        lib.update(connection=self.db, domain=domain)
        
        domain = lib.get_by_internal_id(connection=self.db,
                                        internal_id="internal",
                                        deleted=None,
                                        disabled=False)
        
        self.assertIsNone(domain)
        
        domain = lib.get_by_internal_id(connection=self.db,
                                        internal_id="internal",
                                        deleted=None,
                                        disabled=True)
        
        self.assertIsInstance(domain, Domain)
        self.assertEqual(domain.public_id, "public")
    
    
    def test_get_by_internal_ids(self):
        """
        @summary: Test getting domains by their internal ids.
        """
        
        testDomain = Domain()
        testDomain.root_zone = "example.com"
        testDomain.internal_id = "internal2"
        testDomain.public_id = "public2"
        testDomain.owner_id = "donny"
        testDomain.domain.root_zone = r"example\.com"
        lib.update(connection=self.db, domain=testDomain)
        
        domains = lib.get_by_internal_ids(connection=self.db,
                                          internal_ids=("internal", "internal2"),
                                          deleted=None,
                                          disabled=None)
        
        self.assertEqual(len(domains), 2)
        self.assertIsInstance(domains["internal"], Domain)
        self.assertIsInstance(domains["internal2"], Domain)
        self.assertEqual(domains["internal"].public_id, "public")
        self.assertEqual(domains["internal2"].public_id, "public2")
        
        testDomain.deleted = True
        lib.update(connection=self.db, domain=testDomain)
        
        domains = lib.get_by_internal_ids(connection=self.db,
                                          internal_ids=("internal", "internal2"),
                                          deleted=False,
                                          disabled=None)
        
        self.assertEqual(len(domains), 2)
        self.assertIsInstance(domains["internal"], Domain)
        self.assertIsNone(domains["internal2"])
        self.assertEqual(domains["internal"].public_id, "public")
        
        domains = lib.get_by_internal_ids(connection=self.db,
                                          internal_ids=("internal", "internal2"),
                                          deleted=True,
                                          disabled=None)
        
        self.assertEqual(len(domains), 2)
        self.assertIsNone(domains["internal"])
        self.assertIsInstance(domains["internal2"], Domain)
        self.assertEqual(domains["internal2"].public_id, "public2")
        
        testDomain.deleted = False
        testDomain.disabled = True
        lib.update(connection=self.db, domain=testDomain)
        
        domains = lib.get_by_internal_ids(connection=self.db,
                                          internal_ids=("internal", "internal2"),
                                          deleted=None,
                                          disabled=False)
        
        self.assertEqual(len(domains), 2)
        self.assertIsInstance(domains["internal"], Domain)
        self.assertIsNone(domains["internal2"])
        self.assertEqual(domains["internal"].public_id, "public")
        
        domains = lib.get_by_internal_ids(connection=self.db,
                                          internal_ids=("internal", "internal2"),
                                          deleted=None,
                                          disabled=True)
        
        self.assertEqual(len(domains), 2)
        self.assertIsNone(domains["internal"])
        self.assertIsInstance(domains["internal2"], Domain)
        self.assertEqual(domains["internal2"].public_id, "public2")
    
    
    def test_get_by_public_id(self):
        """
        @summary: Test getting a domain by its public id.
        """
        
        domain = lib.get_by_public_id(connection=self.db,
                                      public_id="public",
                                      deleted=None,
                                      disabled=None)
        
        self.assertIsInstance(domain, Domain)
        self.assertEqual(domain.internal_id, "internal")
        
        domain.deleted = True
        lib.update(connection=self.db, domain=domain)
        
        domain = lib.get_by_public_id(connection=self.db,
                                      public_id="public",
                                      deleted=False,
                                      disabled=None)
        
        self.assertIsNone(domain)
        
        domain = lib.get_by_public_id(connection=self.db,
                                      public_id="public",
                                      deleted=True,
                                      disabled=None)
        
        self.assertIsInstance(domain, Domain)
        self.assertEqual(domain.internal_id, "internal")
        
        domain.deleted = False
        domain.disabled = True
        lib.update(connection=self.db, domain=domain)
        
        domain = lib.get_by_public_id(connection=self.db,
                                      public_id="public",
                                      deleted=None,
                                      disabled=False)
        
        self.assertIsNone(domain)
        
        domain = lib.get_by_public_id(connection=self.db,
                                      public_id="public",
                                      deleted=None,
                                      disabled=True)
        
        self.assertIsInstance(domain, Domain)
        self.assertEqual(domain.internal_id, "internal")
    
    
    def test_get_by_public_ids(self):
        """
        @summary: Test getting domains by their public ids.
        """
        
        testDomain = Domain()
        testDomain.root_zone = "example.com"
        testDomain.internal_id = "internal2"
        testDomain.public_id = "public2"
        testDomain.owner_id = "donny"
        testDomain.domain.root_zone = r"example\.com"
        lib.update(connection=self.db, domain=testDomain)
        
        domains = lib.get_by_public_ids(connection=self.db,
                                        public_ids=("public", "public2"),
                                        deleted=None,
                                        disabled=None)
        
        self.assertEqual(len(domains), 2)
        self.assertIsInstance(domains["public"], Domain)
        self.assertIsInstance(domains["public2"], Domain)
        self.assertEqual(domains["public"].internal_id, "internal")
        self.assertEqual(domains["public2"].internal_id, "internal2")
        
        testDomain.deleted = True
        lib.update(connection=self.db, domain=testDomain)
        
        domains = lib.get_by_public_ids(connection=self.db,
                                        public_ids=("public", "public2"),
                                        deleted=False,
                                        disabled=None)
        
        self.assertEqual(len(domains), 2)
        self.assertIsInstance(domains["public"], Domain)
        self.assertIsNone(domains["public2"])
        self.assertEqual(domains["public"].internal_id, "internal")
        
        domains = lib.get_by_public_ids(connection=self.db,
                                        public_ids=("public", "public2"),
                                        deleted=True,
                                        disabled=None)
        
        self.assertEqual(len(domains), 2)
        self.assertIsNone(domains["public"])
        self.assertIsInstance(domains["public2"], Domain)
        self.assertEqual(domains["public2"].internal_id, "internal2")
        
        testDomain.deleted = False
        testDomain.disabled = True
        lib.update(connection=self.db, domain=testDomain)
        
        domains = lib.get_by_public_ids(connection=self.db,
                                        public_ids=("public", "public2"),
                                        deleted=None,
                                        disabled=False)
        
        self.assertEqual(len(domains), 2)
        self.assertIsInstance(domains["public"], Domain)
        self.assertIsNone(domains["public2"])
        self.assertEqual(domains["public"].internal_id, "internal")
        
        domains = lib.get_by_public_ids(connection=self.db,
                                        public_ids=("public", "public2"),
                                        deleted=None,
                                        disabled=True)
        
        self.assertEqual(len(domains), 2)
        self.assertIsNone(domains["public"])
        self.assertIsInstance(domains["public2"], Domain)
        self.assertEqual(domains["public2"].internal_id, "internal2")
    
    
    def test_get_by_owner_id(self):
        """
        @summary: Test getting domains by its owner's id.
        """
        
        domains = lib.get_by_owner_id(connection=self.db,
                                      owner_id="donny",
                                      deleted=None,
                                      disabled=None)
        
        self.assertIsInstance(domains, set)
        self.assertEqual(len(domains), 1)
        
        domain = domains.pop()
        
        self.assertEqual(domain.internal_id, "internal")
        
        domain.deleted = True
        lib.update(connection=self.db, domain=domain)
        
        domains = lib.get_by_owner_id(connection=self.db,
                                      owner_id="donny",
                                      deleted=False,
                                      disabled=None)
        
        self.assertIsInstance(domains, set)
        self.assertEqual(len(domains), 0)
        
        domains = lib.get_by_owner_id(connection=self.db,
                                      owner_id="donny",
                                      deleted=True,
                                      disabled=None)
        
        self.assertIsInstance(domains, set)
        self.assertEqual(len(domains), 1)
        
        domain = domains.pop()
        
        self.assertEqual(domain.internal_id, "internal")
        
        domain.deleted = False
        domain.disabled = True
        lib.update(connection=self.db, domain=domain)
        
        domains = lib.get_by_owner_id(connection=self.db,
                                      owner_id="donny",
                                      deleted=None,
                                      disabled=False)
        
        self.assertIsInstance(domains, set)
        self.assertEqual(len(domains), 0)
        
        domains = lib.get_by_owner_id(connection=self.db,
                                      owner_id="donny",
                                      deleted=None,
                                      disabled=True)
        
        self.assertIsInstance(domains, set)
        self.assertEqual(len(domains), 1)
        
        domain = domains.pop()
        
        self.assertEqual(domain.internal_id, "internal")
        
    
    
    def test_get_by_owner_ids(self):
        """
        @summary: Test getting domains by its owner's id.
        """
        
        domains = lib.get_by_owner_ids(connection=self.db,
                                      owner_ids=["donny"],
                                      deleted=None,
                                      disabled=None)
        
        self.assertIsInstance(domains, dict)
        self.assertEqual(len(domains), 1)
        
        domain = domains["donny"].pop()
        
        self.assertEqual(domain.internal_id, "internal")
        
        domain.deleted = True
        lib.update(connection=self.db, domain=domain)
        
        domains = lib.get_by_owner_ids(connection=self.db,
                                      owner_ids=["donny"],
                                      deleted=False,
                                      disabled=None)
        
        self.assertIsInstance(domains, dict)
        self.assertEqual(len(domains), 1)
        self.assertEqual(len(domains["donny"]), 0)
        
        domains = lib.get_by_owner_ids(connection=self.db,
                                      owner_ids=["donny"],
                                      deleted=True,
                                      disabled=None)
        
        self.assertIsInstance(domains, dict)
        self.assertEqual(len(domains), 1)
        self.assertEqual(len(domains["donny"]), 1)
        
        domain = domains["donny"].pop()
        
        self.assertEqual(domain.internal_id, "internal")
        
        domain.deleted = False
        domain.disabled = True
        lib.update(connection=self.db, domain=domain)
        
        domains = lib.get_by_owner_ids(connection=self.db,
                                      owner_ids=["donny"],
                                      deleted=None,
                                      disabled=False)
        
        self.assertIsInstance(domains, dict)
        self.assertEqual(len(domains), 1)
        self.assertEqual(len(domains["donny"]), 0)
        
        domains = lib.get_by_owner_ids(connection=self.db,
                                      owner_ids=["donny"],
                                      deleted=None,
                                      disabled=True)
        
        self.assertIsInstance(domains, dict)
        self.assertEqual(len(domains), 1)
        self.assertEqual(len(domains["donny"]), 1)
        
        domain = domains["donny"].pop()
        
        self.assertEqual(domain.internal_id, "internal")
    
    
    def test_get_default_by_owner_id(self):
        """
        @summary: Test getting the default domain by owner id.
        """
        
        testDomain = Domain()
        testDomain.root_zone = "example.com"
        testDomain.internal_id = "internal2"
        testDomain.public_id = "public2"
        testDomain.owner_id = "donny"
        testDomain.default = True
        testDomain.domain.root_zone = r"example\.com"
        lib.update(connection=self.db, domain=testDomain)
        
        domain = lib.get_default_by_owner_id(connection=self.db,
                                             owner_id="donny",
                                             deleted=None,
                                             disabled=None)
        
        self.assertIsInstance(domain, Domain)
        self.assertEqual(domain.internal_id, "internal2")
        
        domain.deleted = True
        lib.update(connection=self.db, domain=domain)
        
        domain = lib.get_default_by_owner_id(connection=self.db,
                                             owner_id="donny",
                                             deleted=False,
                                             disabled=None)
        
        self.assertIsNone(domain)
        
        domain = lib.get_default_by_owner_id(connection=self.db,
                                             owner_id="donny",
                                             deleted=True,
                                             disabled=None)
        
        self.assertIsInstance(domain, Domain)
        self.assertEqual(domain.internal_id, "internal2")
        
        domain.deleted = False
        domain.disabled = True
        lib.update(connection=self.db, domain=domain)
        
        domain = lib.get_default_by_owner_id(connection=self.db,
                                             owner_id="donny",
                                             deleted=None,
                                             disabled=False)
        
        self.assertIsNone(domain)
        
        domain = lib.get_default_by_owner_id(connection=self.db,
                                             owner_id="donny",
                                             deleted=None,
                                             disabled=True)
        
        self.assertIsInstance(domain, Domain)
        self.assertEqual(domain.internal_id, "internal2")
        
    
    def test_get_default_by_owner_ids(self):
        """
        @summary: Test getting default domains for owner ids
        """
        
        testDomain = Domain()
        testDomain.root_zone = "example.com"
        testDomain.internal_id = "internal2"
        testDomain.public_id = "public2"
        testDomain.owner_id = "donny"
        testDomain.default = True
        testDomain.domain.root_zone = r"example\.com"
        lib.update(connection=self.db, domain=testDomain)
        
        testDomain2 = Domain()
        testDomain2.root_zone = "google.com"
        testDomain2.internal_id = "internal3"
        testDomain2.public_id = "public3"
        testDomain2.owner_id = "steve"
        testDomain2.default = True
        testDomain2.domain.root_zone = r"google\.com"
        lib.update(connection=self.db, domain=testDomain2)
        
        domains = lib.get_default_by_owner_ids(connection=self.db,
                                               owner_ids=["donny", "steve"],
                                               deleted=None,
                                               disabled=None)
        
        self.assertIsInstance(domains, dict)
        self.assertEqual(domains["donny"].internal_id, "internal2")
        self.assertEqual(domains["steve"].internal_id, "internal3")
        
        testDomain.deleted = True
        lib.update(connection=self.db, domain=testDomain)
        
        domains = lib.get_default_by_owner_ids(connection=self.db,
                                               owner_ids=["donny", "steve"],
                                               deleted=False,
                                               disabled=None)
        
        self.assertIsInstance(domains, dict)
        self.assertIsNone(domains["donny"])
        self.assertEqual(domains["steve"].internal_id, "internal3")
        
        domains = lib.get_default_by_owner_ids(connection=self.db,
                                               owner_ids=["donny", "steve"],
                                               deleted=True,
                                               disabled=None)
        
        self.assertIsInstance(domains, dict)
        self.assertIsNone(domains["steve"])
        self.assertEqual(domains["donny"].internal_id, "internal2")
        
        testDomain.deleted = False
        testDomain.disabled = True
        lib.update(connection=self.db, domain=testDomain)
        
        domains = lib.get_default_by_owner_ids(connection=self.db,
                                               owner_ids=["donny", "steve"],
                                               deleted=None,
                                               disabled=False)
        
        self.assertIsInstance(domains, dict)
        self.assertIsNone(domains["donny"])
        self.assertEqual(domains["steve"].internal_id, "internal3")
        
        domains = lib.get_default_by_owner_ids(connection=self.db,
                                               owner_ids=["donny", "steve"],
                                               deleted=None,
                                               disabled=True)
        
        self.assertIsInstance(domains, dict)
        self.assertIsNone(domains["steve"])
        self.assertEqual(domains["donny"].internal_id, "internal2")
    
    
    def test_get_by_root_zone(self):
        """
        @summary: Test getting domains by their root zone.
        """
        
        testDomain = Domain()
        testDomain.root_zone = "example.com"
        testDomain.internal_id = "internal2"
        testDomain.public_id = "public2"
        testDomain.owner_id = "donny"
        testDomain.default = False
        testDomain.domain.root_zone = r"example\.com"
        lib.update(connection=self.db, domain=testDomain)
        
        domains = lib.get_by_root_zone(connection=self.db,
                                       root_zone="example.com",
                                       deleted=None,
                                       disabled=None)
        
        self.assertIsInstance(domains, set)
        self.assertEqual(len(domains), 2)
        
        testDomain.deleted = True
        lib.update(connection=self.db, domain=testDomain)
        
        domains = lib.get_by_root_zone(connection=self.db,
                                       root_zone="example.com",
                                       deleted=False,
                                       disabled=None)
        
        self.assertIsInstance(domains, set)
        self.assertEqual(len(domains), 1)
        
        domain = domains.pop()
        
        self.assertEqual(domain.internal_id, "internal")
        
        domains = lib.get_by_root_zone(connection=self.db,
                                       root_zone="example.com",
                                       deleted=True,
                                       disabled=None)
        
        self.assertIsInstance(domains, set)
        self.assertEqual(len(domains), 1)
        
        domain = domains.pop()
        
        self.assertEqual(domain.internal_id, "internal2")
        
        testDomain.deleted = False
        testDomain.disabled = True
        lib.update(connection=self.db, domain=testDomain)
        
        domains = lib.get_by_root_zone(connection=self.db,
                                       root_zone="example.com",
                                       deleted=None,
                                       disabled=False)
        
        self.assertIsInstance(domains, set)
        self.assertEqual(len(domains), 1)
        
        domain = domains.pop()
        
        self.assertEqual(domain.internal_id, "internal")
        
        domains = lib.get_by_root_zone(connection=self.db,
                                       root_zone="example.com",
                                       deleted=None,
                                       disabled=True)
        
        self.assertIsInstance(domains, set)
        self.assertEqual(len(domains), 1)
        
        domain = domains.pop()
        
        self.assertEqual(domain.internal_id, "internal2")
    
    def test_get_by_root_zones(self):
        """
        @summary: Test getting domains by their root zones.
        """
        
        testDomain = Domain()
        testDomain.root_zone = "example.com"
        testDomain.internal_id = "internal2"
        testDomain.public_id = "public2"
        testDomain.owner_id = "donny"
        testDomain.default = False
        testDomain.domain.root_zone = r"example\.com"
        lib.update(connection=self.db, domain=testDomain)
        
        testDomain2 = Domain()
        testDomain2.root_zone = "google.com"
        testDomain2.internal_id = "internal3"
        testDomain2.public_id = "public3"
        testDomain2.owner_id = "steve"
        testDomain2.default = False
        testDomain2.domain.root_zone = r"google\.com"
        lib.update(connection=self.db, domain=testDomain2)
        
        domains = lib.get_by_root_zones(connection=self.db,
                                       root_zones=["example.com", "google.com",
                                                   "justin.tv"],
                                       deleted=None,
                                       disabled=None)
        
        self.assertIsInstance(domains, dict)
        self.assertEqual(len(domains["example.com"]), 2)
        self.assertEqual(len(domains["google.com"]), 1)
        self.assertEqual(len(domains["justin.tv"]), 0)
        
        testDomain.deleted = True
        lib.update(connection=self.db, domain=testDomain)
        
        domains = lib.get_by_root_zones(connection=self.db,
                                       root_zones=["example.com", "google.com",
                                                   "justin.tv"],
                                       deleted=False,
                                       disabled=None)
        
        self.assertIsInstance(domains, dict)
        self.assertEqual(len(domains["example.com"]), 1)
        self.assertEqual(len(domains["google.com"]), 1)
        self.assertEqual(len(domains["justin.tv"]), 0)
        
        domains = lib.get_by_root_zones(connection=self.db,
                                       root_zones=["example.com", "google.com",
                                                   "justin.tv"],
                                       deleted=True,
                                       disabled=None)
        
        self.assertIsInstance(domains, dict)
        self.assertEqual(len(domains["example.com"]), 1)
        self.assertEqual(len(domains["google.com"]), 0)
        self.assertEqual(len(domains["justin.tv"]), 0)
        
        testDomain.deleted = False
        testDomain.disabled = True
        lib.update(connection=self.db, domain=testDomain)
        
        domains = lib.get_by_root_zones(connection=self.db,
                                       root_zones=["example.com", "google.com",
                                                   "justin.tv"],
                                       deleted=None,
                                       disabled=False)
        
        self.assertIsInstance(domains, dict)
        self.assertEqual(len(domains["example.com"]), 1)
        self.assertEqual(len(domains["google.com"]), 1)
        self.assertEqual(len(domains["justin.tv"]), 0)
        
        domains = lib.get_by_root_zones(connection=self.db,
                                       root_zones=["example.com", "google.com",
                                                   "justin.tv"],
                                       deleted=None,
                                       disabled=True)
        
        self.assertIsInstance(domains, dict)
        self.assertEqual(len(domains["example.com"]), 1)
        self.assertEqual(len(domains["google.com"]), 0)
        self.assertEqual(len(domains["justin.tv"]), 0)
    
    
    def test_get_resolves_host(self):
        """
        @summary: Test that a host can be resolved from an iterable of domains
        """
        
        testDomain1 = Domain()
        testDomain1.root_zone = "google.com"
        testDomain1.internal_id = "internal1"
        testDomain1.public_id = "public1"
        testDomain1.owner_id = "donny"
        testDomain1.default = True
        testDomain1.domain.root_zone = r"^google\.com$"
        lib.update(connection=self.db, domain=testDomain1)
        
        testDomain2 = Domain()
        testDomain2.root_zone = "google.com"
        testDomain2.internal_id = "internal2"
        testDomain2.public_id = "public2"
        testDomain2.owner_id = "donny"
        testDomain2.domain.root_zone = r"^google\.com$"
        testDomain2.domain.sub_domain = r"^images$"
        testDomain2.domain.weight = 0.1
        lib.update(connection=self.db, domain=testDomain2)
        
        testDomain3 = Domain()
        testDomain3.root_zone = "example.com"
        testDomain3.internal_id = "internal3"
        testDomain3.public_id = "public3"
        testDomain3.owner_id = "steve"
        testDomain3.default = True
        testDomain3.domain.root_zone = r"^example\.com$"
        testDomain3.domain.weight = 0.1
        lib.update(connection=self.db, domain=testDomain3)
        
        testDomain4 = Domain()
        testDomain4.root_zone = "example.com"
        testDomain4.internal_id = "internal4"
        testDomain4.public_id = "public4"
        testDomain4.owner_id = "steve"
        testDomain4.domain.root_zone = r"^example\.com$"
        testDomain4.domain.sub_domain = r"^docs$"
        testDomain4.domain.weight = 0.2
        lib.update(connection=self.db, domain=testDomain4)
        
        domain = lib.get_resolves_host(connection=self.db,
                                       host="www.google.com",
                                       deleted=None,
                                       disabled=None)
        
        self.assertEqual(domain, testDomain1)
        
        domain = lib.get_resolves_host(connection=self.db,
                                       host="images.google.com",
                                       deleted=None,
                                       disabled=None)
        
        self.assertEqual(domain, testDomain2)
        
        domain = lib.get_resolves_host(connection=self.db,
                                       host="www.example.com",
                                       deleted=None,
                                       disabled=None)
        
        self.assertEqual(domain, testDomain3)
        
        domain = lib.get_resolves_host(connection=self.db,
                                       host="docs.example.com",
                                       deleted=None,
                                       disabled=None)

        self.assertEqual(domain, testDomain4)
        
        testDomain1.deleted = True
        lib.update(connection=self.db, domain=testDomain1)
        
        domain = lib.get_resolves_host(connection=self.db,
                                       host="www.google.com",
                                       deleted=False,
                                       disabled=None)
        
        self.assertIsNone(domain)
        
        domain = lib.get_resolves_host(connection=self.db,
                                       host="www.google.com",
                                       deleted=True,
                                       disabled=None)
        
        self.assertEqual(domain, testDomain1)
        
        testDomain1.deleted = False
        testDomain1.disabled = True
        lib.update(connection=self.db, domain=testDomain1)
        
        domain = lib.get_resolves_host(connection=self.db,
                                       host="www.google.com",
                                       deleted=None,
                                       disabled=False)
        
        self.assertIsNone(domain)
        
        domain = lib.get_resolves_host(connection=self.db,
                                       host="www.google.com",
                                       deleted=None,
                                       disabled=True)
        
        self.assertEqual(domain, testDomain1)
    
    
    def test_set_default_for_root_zone(self):
        """
        @summary: Test setting the default domain for a root zone.
        """
        
        testDomain1 = Domain()
        testDomain1.root_zone = "google.com"
        testDomain1.internal_id = "internal1"
        testDomain1.public_id = "public1"
        testDomain1.owner_id = "donny"
        testDomain1.default = True
        testDomain1.domain.root_zone = r"^google\.com$"
        lib.update(connection=self.db, domain=testDomain1)
        
        testDomain2 = Domain()
        testDomain2.root_zone = "google.com"
        testDomain2.internal_id = "internal2"
        testDomain2.public_id = "public2"
        testDomain2.owner_id = "donny"
        testDomain2.domain.root_zone = r"^google\.com$"
        testDomain2.domain.sub_domain = r"^images$"
        testDomain2.domain.weight = 0.1
        lib.update(connection=self.db, domain=testDomain2)
        
        default_results = lib.set_default_for_root_zone(connection=self.db,
                                        domain=testDomain2,
                                        _fail_implementation=True,
                                        _raise_implementation_exception=False)
        
        self.assertTrue(default_results)
        
        testDomain1 = lib.get_by_internal_id(connection=self.db,
                                         internal_id=testDomain1.internal_id,
                                         deleted=None,
                                         disabled=None)
        
        default_results = lib.set_default_for_root_zone(connection=self.db,
                                        domain=testDomain1,
                                        _fail_implementation=False,
                                        _raise_implementation_exception=False)
        
        self.assertTrue(default_results)
        
        domains = lib.get_by_root_zone(connection=self.db,
                                       root_zone="google.com",
                                       deleted=None,
                                       disabled=None)
        
        for domain in domains:
            if domain.default:
                self.assertEqual(domain, testDomain1)
        
        #failing the non implementation default reset
        default_results = lib.set_default_for_root_zone(connection=self.db,
                                        domain=testDomain1,
                                        _fail_implementation=True,
                                        _raise_implementation_exception=False,
                                        _fail_non_impl_root_zone_set=True)
        
        self.assertFalse(default_results)
        
        testDomain1 = lib.get_by_internal_id(connection=self.db,
                                         internal_id=testDomain1.internal_id,
                                         deleted=None,
                                         disabled=None)
        
        #deleteing the test domain to make the default fail
        lib.delete(connection=self.db, domain=testDomain1, new_default=testDomain2)
        
        default_results = lib.set_default_for_root_zone(connection=self.db,
                                                    domain=testDomain1,
                                                    _fail_implementation=False)
        
        self.assertFalse(default_results)
        
        testDomain1.deleted = False
        lib.update(connection=self.db, domain=testDomain1)
        
        #disabling the test domain to make the default fail
        lib.disable(connection=self.db, domain=testDomain1, new_default=testDomain2)
        
        default_results = lib.set_default_for_root_zone(connection=self.db,
                                                    domain=testDomain1,
                                                    _fail_implementation=False)
        
        self.assertFalse(default_results)
    
    
    def test_disable(self):
        """
        @summary: Test disabling a domain that is currently default and setting
        a new domain to be the default.
        """
        
        testDomain1 = Domain()
        testDomain1.root_zone = "google.com"
        testDomain1.internal_id = "internal1"
        testDomain1.public_id = "public1"
        testDomain1.owner_id = "donny"
        testDomain1.default = True
        testDomain1.domain.root_zone = r"^google\.com$"
        lib.update(connection=self.db, domain=testDomain1)
        
        testDomain2 = Domain()
        testDomain2.root_zone = "google.com"
        testDomain2.internal_id = "internal2"
        testDomain2.public_id = "public2"
        testDomain2.owner_id = "donny"
        testDomain2.domain.root_zone = r"^google\.com$"
        testDomain2.domain.sub_domain = r"^images$"
        testDomain2.domain.weight = 0.1
        lib.update(connection=self.db, domain=testDomain2)
        
        default_results = lib.disable(connection=self.db,
                                      domain=testDomain1,
                                      new_default=testDomain2)
        
        self.assertTrue(default_results)
        
        domains = lib.get_by_root_zone(connection=self.db,
                                       root_zone="google.com",
                                       deleted=None,
                                       disabled=None)
        
        for domain in domains:
            if domain.default:
                self.assertEqual(domain, testDomain2)
            
            elif domain == testDomain1:
                self.assertTrue(domain.disabled)
        
        #disabling an already disabled domain
        default_results = lib.disable(connection=self.db,
                                      domain=testDomain1,
                                      new_default=testDomain2)
        
        self.assertTrue(default_results)
        
        #making it fail because the new default domain will be disabled
        default_results = lib.disable(connection=self.db,
                                      domain=testDomain2,
                                      new_default=testDomain1)
        
        self.assertFalse(default_results)
        
        testDomain1.default = True
        testDomain1.disabled = False
        lib.update(connection=self.db, domain=testDomain1)
        
        testDomain2.default = False
        testDomain2.disabled = False
        lib.update(connection=self.db, domain=testDomain2)
        
        #failing the default call to make testDomain1 remains enabled and
        #default
        default_results = lib.disable(connection=self.db,
                                      domain=testDomain1,
                                      new_default=testDomain2,
                                      _fail_default_root_zone=True)
        
        self.assertFalse(default_results)
        
        domains = lib.get_by_root_zone(connection=self.db,
                                       root_zone="google.com",
                                       deleted=None,
                                       disabled=None)
        
        for domain in domains:
            if domain.default:
                self.assertEqual(domain, testDomain1)
            
            elif domain == testDomain2:
                self.assertFalse(domain.default)
    
    
    def test_delete(self):
        """
        @summary: Test deleting a domain that is currently default and setting
        a new domain to be the default.
        """
        
        testDomain1 = Domain()
        testDomain1.root_zone = "google.com"
        testDomain1.internal_id = "internal1"
        testDomain1.public_id = "public1"
        testDomain1.owner_id = "donny"
        testDomain1.default = True
        testDomain1.domain.root_zone = r"^google\.com$"
        lib.update(connection=self.db, domain=testDomain1)
        
        testDomain2 = Domain()
        testDomain2.root_zone = "google.com"
        testDomain2.internal_id = "internal2"
        testDomain2.public_id = "public2"
        testDomain2.owner_id = "donny"
        testDomain2.domain.root_zone = r"^google\.com$"
        testDomain2.domain.sub_domain = r"^images$"
        testDomain2.domain.weight = 0.1
        lib.update(connection=self.db, domain=testDomain2)
        
        default_results = lib.delete(connection=self.db,
                                      domain=testDomain1,
                                      new_default=testDomain2)
        
        self.assertTrue(default_results)
        
        domains = lib.get_by_root_zone(connection=self.db,
                                       root_zone="google.com",
                                       deleted=None,
                                       disabled=None)
        
        for domain in domains:
            if domain.default:
                self.assertEqual(domain, testDomain2)
            
            elif domain == testDomain1:
                self.assertTrue(domain.deleted)
        
        #deleteing an already deleted domain
        default_results = lib.delete(connection=self.db,
                                      domain=testDomain1,
                                      new_default=testDomain2)
        
        self.assertTrue(default_results)
        
        #making it fail because the new default domain will be disabled
        default_results = lib.delete(connection=self.db,
                                     domain=testDomain2,
                                     new_default=testDomain1)
        
        self.assertFalse(default_results)
        
        testDomain1.default = True
        testDomain1.deleted = False
        lib.update(connection=self.db, domain=testDomain1)
        
        testDomain2.default = False
        testDomain2.deleted = False
        lib.update(connection=self.db, domain=testDomain2)
        
        #failing the default call to make testDomain1 remains enabled and
        #default
        default_results = lib.delete(connection=self.db,
                                     domain=testDomain1,
                                     new_default=testDomain2,
                                     _fail_default_root_zone=True)
        
        self.assertFalse(default_results)
        
        domains = lib.get_by_root_zone(connection=self.db,
                                       root_zone="google.com",
                                       deleted=None,
                                       disabled=None)
        
        for domain in domains:
            if domain.default:
                self.assertEqual(domain, testDomain1)
            
            elif domain == testDomain2:
                self.assertFalse(domain.deleted)