"""
Defines unit test for use with google app engine.
"""


from __future__ import unicode_literals
from domainlib.tests.db.dependent import DependentTestCase
from google.appengine.ext import db
from google.appengine.ext import testbed
from google.appengine.datastore import datastore_stub_util


class GAETestCase(DependentTestCase):
    """
    @summary: Test the google app engine database library implementation.
    
    You can only achieve 89% for domainlib.db.lib.setters because there is no
    app engine specific implementation for setting the default domain.
    """
    
    
    def setUp(self):
        super(GAETestCase, self).setUp()
        
        self.db = db
        self.testBed = testbed.Testbed()
        self.testBed.activate()
        
        # Create a consistency policy that will simulate the High Replication consistency model.
        self.policy = datastore_stub_util.PseudoRandomHRConsistencyPolicy(probability=1)
        # Initialize the datastore stub with this policy.
        self.testBed.init_datastore_v3_stub(consistency_policy=self.policy)
        
        self.postSetUp()
        
    
    def tearDown(self):
        self.testBed.deactivate()
        super(GAETestCase, self).tearDown()