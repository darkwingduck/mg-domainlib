"""
Defines unittest for the non database functions of the db lib.
"""


from __future__ import unicode_literals
import unittest
from domainlib.db.models import Domain
from domainlib.db import lib


class IndependentTestCase(unittest.TestCase):
    """
    @summary: Defines the test cases that do not need the database connection.
    """
    
    
    def test_get_by_internal_id_from_iterable(self):
        """
        @summary: Test getting a Domain from a iterable with the specified
        internal id.
        """
        
        testDomain = Domain()
        testDomain.root_zone = "example.com"
        testDomain.internal_id = "internal"
        testDomain.public_id = "public"
        testDomain.owner_id = "donny"
        testDomain.domain.root_zone = r"example\.com"
        
        domains = set((testDomain,))
        
        found_domain = lib.get_by_internal_id_from_iterable(domains=domains,
                                                      internal_id="internal",
                                                      deleted=None,
                                                      disabled=None)
        
        self.assertIsInstance(found_domain, Domain)
        self.assertEqual(found_domain, testDomain)
        
        found_domain = lib.get_by_internal_id_from_iterable(domains=domains,
                                                      internal_id="internal2",
                                                      deleted=None,
                                                      disabled=None)
        
        self.assertIsNone(found_domain)
        
        testDomain.deleted = True
        
        found_domain = lib.get_by_internal_id_from_iterable(domains=domains,
                                                      internal_id="internal",
                                                      deleted=False,
                                                      disabled=None)
        
        self.assertIsNone(found_domain)
        
        found_domain = lib.get_by_internal_id_from_iterable(domains=domains,
                                                      internal_id="internal",
                                                      deleted=True,
                                                      disabled=None)
        
        self.assertEqual(found_domain, testDomain)
        
        testDomain.deleted = False
        testDomain.disabled = True
        
        found_domain = lib.get_by_internal_id_from_iterable(domains=domains,
                                                      internal_id="internal",
                                                      deleted=None,
                                                      disabled=False)
        
        self.assertIsNone(found_domain)
        
        found_domain = lib.get_by_internal_id_from_iterable(domains=domains,
                                                      internal_id="internal",
                                                      deleted=None,
                                                      disabled=True)
        
        self.assertEqual(found_domain, testDomain)
    
    def test_get_by_internal_ids_from_iterable(self):
        """
        @summary: Test getting a Domain from a iterable with the specified
        internal ids.
        """
        
        testDomain = Domain()
        testDomain.root_zone = "example.com"
        testDomain.internal_id = "internal"
        testDomain.public_id = "public"
        testDomain.owner_id = "donny"
        testDomain.domain.root_zone = r"example\.com"
        
        testDomain2 = Domain()
        testDomain2.root_zone = "example.com"
        testDomain2.internal_id = "internal2"
        testDomain2.public_id = "public2"
        testDomain2.owner_id = "donny"
        testDomain2.domain.root_zone = r"example\.com"
        
        domains = set((testDomain, testDomain2))
        
        found_domains = lib.get_by_internal_ids_from_iterable(domains=domains,
                                      internal_ids=["internal", "internal2"],
                                      deleted=None,
                                      disabled=None)
        
        self.assertIsInstance(found_domains, dict)
        self.assertEqual(found_domains["internal"], testDomain)
        self.assertEqual(found_domains["internal2"], testDomain2)
        
        found_domains = lib.get_by_internal_ids_from_iterable(domains=domains,
                                      internal_ids=["internal3", "internal4"],
                                      deleted=None,
                                      disabled=None)
        
        self.assertIsInstance(found_domains, dict)
        self.assertIsNone(found_domains["internal3"])
        self.assertIsNone(found_domains["internal4"])
        
        testDomain.deleted = True
        found_domains = lib.get_by_internal_ids_from_iterable(domains=domains,
                                      internal_ids=["internal", "internal2"],
                                      deleted=False,
                                      disabled=None)
        
        self.assertIsInstance(found_domains, dict)
        self.assertIsNone(found_domains["internal"])
        self.assertEqual(found_domains["internal2"], testDomain2)
        
        found_domains = lib.get_by_internal_ids_from_iterable(domains=domains,
                                      internal_ids=["internal", "internal2"],
                                      deleted=True,
                                      disabled=None)
        
        self.assertIsInstance(found_domains, dict)
        self.assertIsNone(found_domains["internal2"])
        self.assertEqual(found_domains["internal"], testDomain)
        
        testDomain.deleted = False
        testDomain.disabled = True
        found_domains = lib.get_by_internal_ids_from_iterable(domains=domains,
                                      internal_ids=["internal", "internal2"],
                                      deleted=None,
                                      disabled=False)
        
        self.assertIsInstance(found_domains, dict)
        self.assertIsNone(found_domains["internal"])
        self.assertEqual(found_domains["internal2"], testDomain2)
        
        found_domains = lib.get_by_internal_ids_from_iterable(domains=domains,
                                      internal_ids=["internal", "internal2"],
                                      deleted=None,
                                      disabled=True)
        
        self.assertIsInstance(found_domains, dict)
        self.assertIsNone(found_domains["internal2"])
        self.assertEqual(found_domains["internal"], testDomain)
    
    
    def test_get_by_public_id_from_iterable(self):
        """
        @summary: Test getting a Domain from a iterable with the specified
        public id.
        """
        
        testDomain = Domain()
        testDomain.root_zone = "example.com"
        testDomain.internal_id = "internal"
        testDomain.public_id = "public"
        testDomain.owner_id = "donny"
        testDomain.domain.root_zone = r"example\.com"
        
        domains = set((testDomain,))
        
        found_domain = lib.get_by_public_id_from_iterable(domains=domains,
                                                      public_id="public",
                                                      deleted=None,
                                                      disabled=None)
        
        self.assertEqual(found_domain, testDomain)
        
        found_domain = lib.get_by_public_id_from_iterable(domains=domains,
                                                      public_id="public2",
                                                      deleted=None,
                                                      disabled=None)
        
        self.assertIsNone(found_domain)
        
        testDomain.deleted = True
        found_domain = lib.get_by_public_id_from_iterable(domains=domains,
                                                      public_id="public",
                                                      deleted=False,
                                                      disabled=None)
        
        self.assertIsNone(found_domain)
        
        found_domain = lib.get_by_public_id_from_iterable(domains=domains,
                                                      public_id="public",
                                                      deleted=True,
                                                      disabled=None)
        
        self.assertEqual(found_domain, testDomain)
        
        testDomain.deleted = False
        testDomain.disabled = True
        found_domain = lib.get_by_public_id_from_iterable(domains=domains,
                                                      public_id="public",
                                                      deleted=None,
                                                      disabled=False)
        
        self.assertIsNone(found_domain)
        
        found_domain = lib.get_by_public_id_from_iterable(domains=domains,
                                                      public_id="public",
                                                      deleted=None,
                                                      disabled=True)
        
        self.assertEqual(found_domain, testDomain)
        
    
    def test_get_by_public_ids_from_iterable(self):
        """
        @summary: Test getting a Domain from a iterable with the specified
        public ids.
        """
        
        testDomain = Domain()
        testDomain.root_zone = "example.com"
        testDomain.internal_id = "internal"
        testDomain.public_id = "public"
        testDomain.owner_id = "donny"
        testDomain.domain.root_zone = r"example\.com"
        
        testDomain2 = Domain()
        testDomain2.root_zone = "example.com"
        testDomain2.internal_id = "internal2"
        testDomain2.public_id = "public2"
        testDomain2.owner_id = "donny"
        testDomain2.domain.root_zone = r"example\.com"
        
        domains = set((testDomain, testDomain2))
        
        found_domains = lib.get_by_public_ids_from_iterable(domains=domains,
                                      public_ids=["public", "public2"],
                                      deleted=None,
                                      disabled=None)
        
        self.assertIsInstance(found_domains, dict)
        self.assertEqual(found_domains["public"], testDomain)
        self.assertEqual(found_domains["public2"], testDomain2)
        
        found_domains = lib.get_by_public_ids_from_iterable(domains=domains,
                                      public_ids=["public3", "public4"],
                                      deleted=None,
                                      disabled=None)
        
        self.assertIsInstance(found_domains, dict)
        self.assertIsNone(found_domains["public3"])
        self.assertIsNone(found_domains["public4"])
        
        testDomain.deleted = True
        
        found_domains = lib.get_by_public_ids_from_iterable(domains=domains,
                                      public_ids=["public", "public2"],
                                      deleted=False,
                                      disabled=None)
        
        self.assertIsInstance(found_domains, dict)
        self.assertIsNone(found_domains["public"])
        self.assertEqual(found_domains["public2"], testDomain2)
        
        found_domains = lib.get_by_public_ids_from_iterable(domains=domains,
                                      public_ids=["public", "public2"],
                                      deleted=True,
                                      disabled=None)
        
        self.assertIsInstance(found_domains, dict)
        self.assertIsNone(found_domains["public2"])
        self.assertEqual(found_domains["public"], testDomain)
        
        testDomain.deleted = False
        testDomain.disabled = True
        
        found_domains = lib.get_by_public_ids_from_iterable(domains=domains,
                                      public_ids=["public", "public2"],
                                      deleted=None,
                                      disabled=False)
        
        self.assertIsInstance(found_domains, dict)
        self.assertIsNone(found_domains["public"])
        self.assertEqual(found_domains["public2"], testDomain2)
        
        found_domains = lib.get_by_public_ids_from_iterable(domains=domains,
                                      public_ids=["public", "public2"],
                                      deleted=None,
                                      disabled=True)
        
        self.assertIsInstance(found_domains, dict)
        self.assertIsNone(found_domains["public2"])
        self.assertEqual(found_domains["public"], testDomain)
        
    
    def test_get_by_owner_id_from_iterable(self):
        """
        @summary: Test getting a Domain from a iterable with the specified
        owner id.
        """
        
        testDomain = Domain()
        testDomain.root_zone = "example.com"
        testDomain.internal_id = "internal"
        testDomain.public_id = "public"
        testDomain.owner_id = "donny"
        testDomain.domain.root_zone = r"example\.com"
        
        domains = set((testDomain,))
        
        found_domains = lib.get_by_owner_id_from_iterable(domains=domains,
                                                         owner_id="donny",
                                                         deleted=None,
                                                         disabled=None)
        
        self.assertIsInstance(found_domains, set)
        
        domain = found_domains.pop()
        
        self.assertEqual(domain, testDomain)
        
        found_domains = lib.get_by_owner_id_from_iterable(domains=domains,
                                                         owner_id="steve",
                                                         deleted=None,
                                                         disabled=None)
        
        self.assertIsInstance(found_domains, set)
        self.assertEqual(len(found_domains), 0)
        
        testDomain.deleted = True
        
        found_domains = lib.get_by_owner_id_from_iterable(domains=domains,
                                                         owner_id="donny",
                                                         deleted=False,
                                                         disabled=None)
        
        self.assertIsInstance(found_domains, set)
        self.assertEqual(len(found_domains), 0)
        
        found_domains = lib.get_by_owner_id_from_iterable(domains=domains,
                                                         owner_id="donny",
                                                         deleted=True,
                                                         disabled=None)
        
        self.assertIsInstance(found_domains, set)
        self.assertEqual(len(found_domains), 1)
        
        domain = found_domains.pop()
        
        self.assertEqual(domain, testDomain)
        
        testDomain.deleted = False
        testDomain.disabled = True
        
        found_domains = lib.get_by_owner_id_from_iterable(domains=domains,
                                                         owner_id="donny",
                                                         deleted=None,
                                                         disabled=False)
        
        self.assertIsInstance(found_domains, set)
        self.assertEqual(len(found_domains), 0)
        
        found_domains = lib.get_by_owner_id_from_iterable(domains=domains,
                                                         owner_id="donny",
                                                         deleted=None,
                                                         disabled=True)
        
        self.assertIsInstance(found_domains, set)
        self.assertEqual(len(found_domains), 1)
        
        domain = found_domains.pop()
        
        self.assertEqual(domain, testDomain)
        
    
    def test_get_by_owner_ids_from_iterable(self):
        """
        @summary: Test getting a Domain from a iterable with the specified
        owner ids.
        """
        
        testDomain = Domain()
        testDomain.root_zone = "example.com"
        testDomain.internal_id = "internal"
        testDomain.public_id = "public"
        testDomain.owner_id = "donny"
        testDomain.domain.root_zone = r"example\.com"
        
        testDomain2 = Domain()
        testDomain2.root_zone = "example.com"
        testDomain2.internal_id = "internal2"
        testDomain2.public_id = "public2"
        testDomain2.owner_id = "steve"
        testDomain2.domain.root_zone = r"example\.com"
        
        domains = set((testDomain, testDomain2))
        
        found_domains = lib.get_by_owner_ids_from_iterable(domains=domains,
                                      owner_ids=["donny", "steve"],
                                      deleted=None,
                                      disabled=None)
        
        self.assertIsInstance(found_domains, dict)
        
        donny_domain = found_domains["donny"].pop()
        steve_domain = found_domains["steve"].pop()
        
        self.assertEqual(donny_domain, testDomain)
        self.assertEqual(steve_domain, testDomain2)
        
        found_domains = lib.get_by_owner_ids_from_iterable(domains=domains,
                                      owner_ids=["gary", "bob"],
                                      deleted=None,
                                      disabled=None)
        
        self.assertIsInstance(found_domains, dict)
        
        self.assertEqual(len(found_domains["gary"]), 0)
        self.assertEqual(len(found_domains["bob"]), 0)
        
        testDomain.deleted = True
        
        found_domains = lib.get_by_owner_ids_from_iterable(domains=domains,
                                      owner_ids=["donny", "steve"],
                                      deleted=False,
                                      disabled=None)
        
        self.assertIsInstance(found_domains, dict)
        
        steve_domain = found_domains["steve"].pop()
        
        self.assertEqual(len(found_domains["donny"]), 0)
        self.assertEqual(steve_domain, testDomain2)
        
        found_domains = lib.get_by_owner_ids_from_iterable(domains=domains,
                                      owner_ids=["donny", "steve"],
                                      deleted=True,
                                      disabled=None)
        
        self.assertIsInstance(found_domains, dict)
        
        donny_domain = found_domains["donny"].pop()
        
        self.assertEqual(len(found_domains["steve"]), 0)
        self.assertEqual(donny_domain, testDomain)
        
        testDomain.deleted = False
        testDomain.disabled = True
        
        found_domains = lib.get_by_owner_ids_from_iterable(domains=domains,
                                      owner_ids=["donny", "steve"],
                                      deleted=None,
                                      disabled=False)
        
        self.assertIsInstance(found_domains, dict)
        
        steve_domain = found_domains["steve"].pop()
        
        self.assertEqual(len(found_domains["donny"]), 0)
        self.assertEqual(steve_domain, testDomain2)
        
        found_domains = lib.get_by_owner_ids_from_iterable(domains=domains,
                                      owner_ids=["donny", "steve"],
                                      deleted=None,
                                      disabled=True)
        
        self.assertIsInstance(found_domains, dict)
        
        donny_domain = found_domains["donny"].pop()
        
        self.assertEqual(len(found_domains["steve"]), 0)
        self.assertEqual(donny_domain, testDomain)
    
    
    def test_get_default_by_owner_id_from_iterable(self):
        """
        @summary: Test getting the default Domain from a iterable with the
        specified owner id.
        """
        
        testDomain = Domain()
        testDomain.root_zone = "example.com"
        testDomain.internal_id = "internal"
        testDomain.public_id = "public"
        testDomain.owner_id = "donny"
        testDomain.domain.root_zone = r"example\.com"
        
        testDomain2 = Domain()
        testDomain2.root_zone = "example.com"
        testDomain2.internal_id = "internal2"
        testDomain2.public_id = "public2"
        testDomain2.owner_id = "donny"
        testDomain2.default = True
        testDomain2.domain.root_zone = r"example\.com"
        
        domains = set((testDomain, testDomain2))
        
        domain = lib.get_default_by_owner_id_from_iterable(domains=domains,
                                                           owner_id="donny",
                                                           deleted=None,
                                                           disabled=None)
        
        self.assertIsInstance(domain, Domain)
        self.assertEqual(domain, testDomain2)
        
        domain = lib.get_default_by_owner_id_from_iterable(domains=domains,
                                                           owner_id="steve",
                                                           deleted=None,
                                                           disabled=None)
        
        self.assertIsNone(domain)
        
        testDomain2.deleted = True
        
        domain = lib.get_default_by_owner_id_from_iterable(domains=domains,
                                                           owner_id="donny",
                                                           deleted=False,
                                                           disabled=None)
        
        self.assertIsNone(domain)
        
        domain = lib.get_default_by_owner_id_from_iterable(domains=domains,
                                                           owner_id="donny",
                                                           deleted=True,
                                                           disabled=None)
        
        self.assertIsInstance(domain, Domain)
        self.assertEqual(domain, testDomain2)
        
        testDomain2.deleted = False
        testDomain2.disabled = True
        
        domain = lib.get_default_by_owner_id_from_iterable(domains=domains,
                                                           owner_id="donny",
                                                           deleted=None,
                                                           disabled=False)
        
        self.assertIsNone(domain)
        
        domain = lib.get_default_by_owner_id_from_iterable(domains=domains,
                                                           owner_id="donny",
                                                           deleted=None,
                                                           disabled=True)
        
        self.assertIsInstance(domain, Domain)
        self.assertEqual(domain, testDomain2)
        
    
    def test_get_default_by_owner_ids_from_iterable(self):
        """
        @summary: Test getting the default Domain from a iterable with the
        specified owner ids.
        """
        
        testDomain = Domain()
        testDomain.root_zone = "example.com"
        testDomain.internal_id = "internal"
        testDomain.public_id = "public"
        testDomain.owner_id = "donny"
        testDomain.domain.root_zone = r"example\.com"
        
        testDomain2 = Domain()
        testDomain2.root_zone = "example.com"
        testDomain2.internal_id = "internal2"
        testDomain2.public_id = "public2"
        testDomain2.owner_id = "donny"
        testDomain2.default = True
        testDomain2.domain.root_zone = r"example\.com"
        
        testDomain3 = Domain()
        testDomain3.root_zone = "example.com"
        testDomain3.internal_id = "internal3"
        testDomain3.public_id = "public3"
        testDomain3.owner_id = "steve"
        testDomain3.domain.root_zone = r"example\.com"
        
        testDomain4 = Domain()
        testDomain4.root_zone = "example.com"
        testDomain4.internal_id = "internal4"
        testDomain4.public_id = "public4"
        testDomain4.owner_id = "steve"
        testDomain4.default = True
        testDomain4.domain.root_zone = r"example\.com"
        
        domains = set((testDomain, testDomain2, testDomain3, testDomain4))
        
        found_domains = lib.get_default_by_owner_ids_from_iterable(
                                                   domains=domains,
                                                   owner_ids=["donny", "steve"],
                                                   deleted=None,
                                                   disabled=None)
        
        self.assertIsInstance(found_domains, dict)
        
        self.assertEqual(found_domains["donny"], testDomain2)
        self.assertEqual(found_domains["steve"], testDomain4)
        
        found_domains = lib.get_default_by_owner_ids_from_iterable(
                                                   domains=domains,
                                                   owner_ids=["gary", "bob"],
                                                   deleted=None,
                                                   disabled=None)
        
        self.assertIsInstance(found_domains, dict)
        
        self.assertIsNone(found_domains["gary"])
        self.assertIsNone(found_domains["bob"])
        
        testDomain2.deleted = True
        
        found_domains = lib.get_default_by_owner_ids_from_iterable(
                                                   domains=domains,
                                                   owner_ids=["donny", "steve"],
                                                   deleted=False,
                                                   disabled=None)
        
        self.assertIsInstance(found_domains, dict)
        
        self.assertIsNone(found_domains["donny"])
        self.assertEqual(found_domains["steve"], testDomain4)
        
        found_domains = lib.get_default_by_owner_ids_from_iterable(
                                                   domains=domains,
                                                   owner_ids=["donny", "steve"],
                                                   deleted=True,
                                                   disabled=None)
        
        self.assertIsInstance(found_domains, dict)
        
        self.assertIsNone(found_domains["steve"])
        self.assertEqual(found_domains["donny"], testDomain2)
        
        testDomain2.deleted = False
        testDomain2.disabled = True
        
        found_domains = lib.get_default_by_owner_ids_from_iterable(
                                                   domains=domains,
                                                   owner_ids=["donny", "steve"],
                                                   deleted=None,
                                                   disabled=False)
        
        self.assertIsInstance(found_domains, dict)
        
        self.assertIsNone(found_domains["donny"])
        self.assertEqual(found_domains["steve"], testDomain4)
        
        found_domains = lib.get_default_by_owner_ids_from_iterable(
                                                   domains=domains,
                                                   owner_ids=["donny", "steve"],
                                                   deleted=None,
                                                   disabled=True)
        
        self.assertIsInstance(found_domains, dict)
        
        self.assertIsNone(found_domains["steve"])
        self.assertEqual(found_domains["donny"], testDomain2)
        
    
    
    def test_no_db_implementation(self):
        """
        @summary: Calls all the methods that require a valid database
        implementation with something that doesn't implement a database
        """
        
        self.assertIsNone(lib.determine_db(connection=str))
        
        self.assertFalse(lib.update(connection=str, domain=None))
        
        self.assertIsNone(lib.get_by_internal_id(connection=str,
                                                 internal_id="bob"))
        
        test_ids = ["bob"]
        
        found = lib.get_by_internal_ids(connection=str, internal_ids=test_ids)
        
        self.assertIsInstance(found, dict)
        
        for key, value in found.items():
            self.assertIn(key, test_ids)
            self.assertIsNone(value)
        
        self.assertIsNone(lib.get_by_public_id(connection=str, public_id="bob"))
        
        found = lib.get_by_public_ids(connection=str, public_ids=test_ids)
        
        self.assertIsInstance(found, dict)
        
        for key, value in found.items():
            self.assertIn(key, test_ids)
            self.assertIsNone(value)
        
        found = lib.get_by_owner_id(connection=str, owner_id="bob")
        
        self.assertIsInstance(found, set)
        self.assertEqual(len(found), 0)
        
        found = lib.get_by_owner_ids(connection=str, owner_ids=test_ids)
        
        self.assertIsInstance(found, dict)
        
        for key, value in found.items():
            self.assertIn(key, test_ids)
            self.assertIsInstance(value, set)
            self.assertEqual(len(value), 0)
        
        found = lib.get_default_by_owner_id(connection=str, owner_id="bob")
        
        self.assertIsInstance(found, set)
        self.assertEqual(len(found), 0)
        
        found = lib.get_default_by_owner_ids(connection=str, owner_ids=test_ids)
        
        self.assertIsInstance(found, dict)
        
        for key, value in found.items():
            self.assertIn(key, test_ids)
            self.assertIsNone(value)
        
        found = lib.get_by_root_zone(connection=str, root_zone="bob")
        
        self.assertIsInstance(found, set)
        self.assertEqual(len(found), 0)
        
        found = lib.get_by_root_zones(connection=str, root_zones=test_ids)
        
        self.assertIsInstance(found, dict)
        
        for key, value in found.items():
            self.assertIn(key, test_ids)
            self.assertIsInstance(value, set)
            self.assertEqual(len(value), 0)
    
    
    def test_get_resolves_host_from_iterable(self):
        """
        @summary: Test that a host can be resolved from an iterable of domains
        """
        
        testDomain1 = Domain()
        testDomain1.root_zone = "google.com"
        testDomain1.internal_id = "internal1"
        testDomain1.public_id = "public1"
        testDomain1.owner_id = "donny"
        testDomain1.default = True
        testDomain1.domain.root_zone = r"^google\.com$"
        
        testDomain2 = Domain()
        testDomain2.root_zone = "google.com"
        testDomain2.internal_id = "internal2"
        testDomain2.public_id = "public2"
        testDomain2.owner_id = "donny"
        testDomain2.domain.root_zone = r"^google\.com$"
        testDomain2.domain.sub_domain = r"^images$"
        testDomain2.domain.weight = 0.1
        
        testDomain3 = Domain()
        testDomain3.root_zone = "example.com"
        testDomain3.internal_id = "internal3"
        testDomain3.public_id = "public3"
        testDomain3.owner_id = "steve"
        testDomain3.default = True
        testDomain3.domain.root_zone = r"^example\.com$"
        
        testDomain4 = Domain()
        testDomain4.root_zone = "example.com"
        testDomain4.internal_id = "internal4"
        testDomain4.public_id = "public4"
        testDomain4.owner_id = "steve"
        testDomain4.domain.root_zone = r"^example\.com$"
        testDomain4.domain.sub_domain = r"^docs$"
        
        domains = (testDomain1, testDomain2, testDomain3, testDomain4,)
        
        domain = lib.get_resolves_host_from_iterable(domains=domains,
                                                     host="www.google.com",
                                                     deleted=None,
                                                     disabled=None)
        
        self.assertEqual(domain, testDomain1)
        
        domain = lib.get_resolves_host_from_iterable(domains=domains,
                                                     host="images.google.com",
                                                     deleted=None,
                                                     disabled=None)
        
        self.assertEqual(domain, testDomain2)
        
        domain = lib.get_resolves_host_from_iterable(domains=domains,
                                                     host="www.example.com",
                                                     deleted=None,
                                                     disabled=None)
        
        self.assertEqual(domain, testDomain3)
        
        domain = lib.get_resolves_host_from_iterable(domains=domains,
                                                     host="docs.example.com",
                                                     deleted=None,
                                                     disabled=None)
        
        self.assertEqual(domain, testDomain4)
        
        testDomain1.deleted = True
        
        domain = lib.get_resolves_host_from_iterable(domains=domains,
                                                     host="www.google.com",
                                                     deleted=False,
                                                     disabled=None)
        
        self.assertIsNone(domain)
        
        domain = lib.get_resolves_host_from_iterable(domains=domains,
                                                     host="www.google.com",
                                                     deleted=True,
                                                     disabled=None)
        
        self.assertEqual(domain, testDomain1)
        
        testDomain1.deleted = False
        testDomain1.disabled = True
        
        domain = lib.get_resolves_host_from_iterable(domains=domains,
                                                     host="www.google.com",
                                                     deleted=None,
                                                     disabled=False)
        
        self.assertIsNone(domain)
        
        domain = lib.get_resolves_host_from_iterable(domains=domains,
                                                     host="www.google.com",
                                                     deleted=None,
                                                     disabled=True)
        
        self.assertEqual(domain, testDomain1)