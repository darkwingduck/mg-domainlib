"""
Defines unittest for dominstlib.domain.lib
"""


from __future__ import unicode_literals
import unittest
from domainlib.domain import Domain
from domainlib.lib import find_domain, split_domain


class LibTestCase(unittest.TestCase):
    def test_find_domain(self):
        """
        @summary: Test finding a Domain instance from a iterable of Domain
        instances using a string domain.
        """
        
        dom_1 = Domain(root_zone=r"^example\.com$",
                       sub_domain=r"^\w*$",
                       weight=0.0)
        
        dom_2 = Domain(root_zone=r"^example\.com$",
                       sub_domain=r"(^\w*\.|^)images$",
                       weight=0.1)
        
        dom_3 = Domain(root_zone=r"^example\.com$",
                       sub_domain=r"^steve$",
                       weight=0.1)
        
        domains = (dom_1, dom_2, dom_3)
        
        found_domain = find_domain("http://www.example.com:8080/buddy",
                                   domains=domains,
                                   default=None)
        
        self.assertEqual(found_domain, dom_1)
        
        found_domain = find_domain("http://example.com:8080/buddy",
                                   domains=domains,
                                   default=None)
        
        self.assertEqual(found_domain, dom_1)
        
        found_domain = find_domain("http://images.example.com:8000",
                                   domains=domains,
                                   default=None)
        
        self.assertEqual(found_domain, dom_2)
        
        found_domain = find_domain("http://steve.images.example.com",
                                   domains=domains,
                                   default=None)
        
        self.assertEqual(found_domain, dom_2)
        
        found_domain = find_domain("http://steve.example.com:80",
                                   domains=domains,
                                   default=None)
        
        self.assertEqual(found_domain, dom_3)
        
        found_domain = find_domain("http://penguin.com",
                                   domains=domains,
                                   default=None)
        
        self.assertIsNone(found_domain)
    
    
    def test_split_domain(self):
        """
        @summary: Test that splitting a domain into its sub domain, root zone,
        and port works correctly.
        """
        
        sub_domain, root_zone, port = split_domain(
                                   domain="http://www.example.com:8080/buddy")
        
        self.assertEqual(sub_domain, "www")
        self.assertEqual(root_zone, "example.com")
        self.assertEqual(port, "8080")
        
        sub_domain, root_zone, port = split_domain(
                                   domain="http://example.com/buddy")
        
        self.assertEqual(sub_domain, "")
        self.assertEqual(root_zone, "example.com")
        self.assertEqual(port, "")